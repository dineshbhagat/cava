Cava: Clean Java
=================
Enable Java programmers to write clean, minimal, simple code

Principles
=================

RunTimeExceptions over Checked Exceptions
-----------------------------------------
Robert Martin argues in Clean Code that we should always use Unchecked Exceptions, and we agree with him.

Suppose for example that you're reading from a file, using the Apache FileUtils library. The library method throws an IOException, forcing you to deal with it as well. But very often, it is perfectly valid to say *"If I ever encounter an error while reading this file, I will let the application exit immediately, or I will let the exception escalate to the outermost main method of my application, where all exceptions will be caught and handled by a generic handler."* If the IOException being thrown was a RunTimeException, this behavior automatically happens. But because the IOException is a checked exception, this forces you to pollute your code with try-catch blocks or throws declarations, throughout your codebase.
 
There are hundreds of possible reason for any code to fail, and programmers should have the flexibility to decide which of these reasons should be caught and handled programmatically, and which of them should be silently escalated to a system exit or generic exception handler. This is a decision that every application should make for itself, without being forced into it by an external library. Hence why we strive to provide library alternatives that are free of Checked Exceptions.
 
Simplify Common Code Paths
--------------------------
There are many generic code paths that thousands of programmers around the world are manually implementing individually, over and over again. This is a violation of DRY on a global level, and an unnecessary burden that makes programming less enjoyable. Cava aims to allieviate this problem by providing these common code paths in a library.

Keep It Simple
--------------
We don't believe in magic. We don't believe in doing anything smart or clever. We simply want to write library functions that are simple, clear and intuitive. If another library already does 90% of what we need, we will use that library ourselves.

Our goal is not performance. We will never optimize our implementations or interfaces for performance. Our goal is first and foremost to provide the user with the most convenient utility for their needs, and second, to implement this interface in a manner that makes maintenance as easy as possible.

Planned Obsolescence
--------------
In an ideal world, other libraries like ApacheCommons and Guava will make us obsolete. Whenever we find another library that is as clean and friendly as our own, we will deprecate our own implementation and refer our users to them instead. We exist only to cover up for others' failings, and we yearn for the day when this library will no longer be needed.


Features
=========

Validate external inputs and program state: Better error messages and user-specified exception in single line
-------------------------------------------------------------------------------------------------------------
Replace:
```java
if (!a.equals(b)) {
    log.error("A: " + a + ", B: " + b);
    throw new MyCustomException("Error!");
}
```

with:
```java
// Exception will be thrown only if check fails
Validatec.equals(a, b, new MyCustomException("Error!"));
```

or:
```java
// Exception will be constructed & thrown only if check fails
Validatec.equals(a, b, MyCustomException.class, "Error!");
```

Convert any checked exceptions to run-time exceptions without altering the stack-trace
----------------------------------------------------------------------------------------------
Replace:  
```java
try {
    method();
} catch (CheckedException e) {
    throw new RunTimeException(e);              // stack-trace and cause modified, with noisy data
}
```

With:  
```java
try {
    method();
} catch (CheckedException e) {
    throw new CheckedExceptionWrapper(e);       // getStackTrace, getMessage, getCause will return *exact same values*
}
```

FileUtils, IOUtils, ThreadUtils: Checked-Exceptions converted to RunTimExceptions
-------------
CheckedExceptionWrapper is used to ensure no loss or corruption of stack-trace info.

Replace:  
```java
try {
    String fileContents = FileUtils.readFileToString(file);
} catch (IOException e) {
    throw new RunTimeException(e);
}
```
    
with:  
```java
// IoException will be thrown if it fails (RunTimException, not CheckedException)
String fileContents = FileUtilc.readFileToString(file);
```

Replace:  
```java
try {
    Thread.sleep(10);
} catch (InterruptedException e) {
    throw new RunTimeException(e);
}
```

with:  
```java
// InterruptException will be thrown if it fails (RunTimException, not CheckedException)
ThreadUtilc.sleep(10);
```

Maven Installation
==================

Add the following to your pom.xml file, and you should be good to go. Check [here for latest version](https://gitlab.com/whacks/cava/blob/master/pom.xml).

```xml
<dependencies>
    <dependency>
        <groupId>com.rajivprab</groupId>
        <artifactId>cava</artifactId>
        <version>2.0.4</version>
    </dependency>
</dependencies>
```

About
=====
Cava was developed while building [Caucus](thecaucus.net). We realized that we had a number of utility functions and code patterns that we were manually implementing over and over again, in countless instances. These were all borne out of certain frustrations and limitations of the 3rd party libraries that we use.

We initially just grumbled and put up with it, but soon decided that there's no reason why we couldn't abstract out these repeated code patterns into a library of our own. A library that aims to make Java cleaner and more elegant to program in. A library generic enough that anyone can use it, and simple enough that anyone can understand and maintain it. Thus was born Cava.