package org.rajivprab.cava;

import com.google.common.truth.Truth;
import org.junit.Test;

import java.time.Duration;
import java.time.Instant;

public class ComparablecTest extends TestBase {
    @Test
    public void isGreater_shouldReturnTrue() {
        Truth.assertThat(Comparablec.isGreater(1.1, 1.01)).isTrue();
        Truth.assertThat(Comparablec.isGreaterOrEqual(11, 10)).isTrue();
    }

    @Test
    public void isLessThan() {
        Truth.assertThat(Comparablec.isGreater(Duration.ofSeconds(2), Duration.ofMinutes(1))).isFalse();
        Truth.assertThat(Comparablec.isGreaterOrEqual(Duration.ofSeconds(2), Duration.ofMinutes(1))).isFalse();
    }

    @Test
    public void isExactlyEqual() {
        Instant now = Instant.now();
        Truth.assertThat(Comparablec.isGreater(now, now)).isFalse();
        Truth.assertThat(Comparablec.isGreaterOrEqual(now, now)).isTrue();
    }
}
