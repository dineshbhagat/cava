package org.rajivprab.cava;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.truth.Truth;
import org.junit.Test;

import java.util.Map;

/**
 * Unit tests for Mapc utilities
 *
 * Created by rajivprab on 1/16/17.
 */
public class MapcTest extends TestBase {
    @Test
    public void constructMapFiveEntries_allGood() {
        Map<Integer, String> map = Mapc.newHashMap(1, "one",
                                                   2, "two",
                                                   3, "three",
                                                   4, "four",
                                                   5, "five");
        Truth.assertThat(map).hasSize(5);
        Truth.assertThat(map).containsEntry(1, "one");
        Truth.assertThat(map).containsEntry(2, "two");
        Truth.assertThat(map).containsEntry(3, "three");
        Truth.assertThat(map).containsEntry(4, "four");
        Truth.assertThat(map).containsEntry(5, "five");
    }

    @Test
    public void cacheGetWithHit() {
        Cache<Integer, String> cache = CacheBuilder.newBuilder().build();
        cache.put(1, "one");
        Truth.assertThat(Mapc.cacheGet(cache, 1, () -> "bad")).isEqualTo("one");
    }

    @Test
    public void cacheGetWithMiss() {
        String key = "1";
        Truth.assertThat(Mapc.cacheGet(CacheBuilder.newBuilder().build(), key, () -> Integer.valueOf(key))).isEqualTo(1);
    }

    @Test (expected = NullPointerException.class)
    public void cacheGetNullLambda_throwsNullPointerException() {
        Mapc.cacheGet(CacheBuilder.newBuilder().build(), 1, null);
    }

    @Test (expected = NullPointerException.class)
    public void cacheGetNullKey_throwsNullPointerException() {
        Mapc.cacheGet(CacheBuilder.newBuilder().build(), null, () -> "bad");
    }

    @Test
    public void cacheGet_withMiss_CallableThrowsException() {
        try {
            Mapc.cacheGet(CacheBuilder.newBuilder().build(), 1, () -> {
                throw new ArithmeticException("test");
            });
        } catch (ArithmeticException e) {
            Truth.assertThat(e).hasMessageThat().contains("test");
        }
    }
}
