package org.rajivprab.cava;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Ignore;

/**
 * General config that might be needed by all other unit tests
 *
 * Created by rprabhakar on 1/31/16.
 */
public class TestBase {
    protected static final Logger log = LogManager.getLogger(TestBase.class);

    @Ignore
    public void loggerShouldPrintInfoAndAbove() {
        log.fatal("fatal message");
        log.error("error message");
        log.warn("warn message");
        log.info("info message");
        log.debug("debug message - SHOULD NOT BE PRINTED");
        log.trace("trace message - SHOULD NOT BE PRINTED");
    }
}
