package org.rajivprab.cava;

import com.google.common.truth.Truth;
import org.junit.Test;

/**
 * Unit tests for IOUtilc utilities
 *
 * Created by rajivprab on 5/18/17.
 */
public class IOUtilcTest extends TestBase {
    @Test
    public void readLines() {
        Truth.assertThat(IOUtilc.readLines(ClassLoader.getSystemResourceAsStream("sample_file.txt")))
             .containsExactly("sample file", "line 2", "line 3");
    }
}
