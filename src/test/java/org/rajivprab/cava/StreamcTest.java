package org.rajivprab.cava;

import com.google.common.collect.Lists;
import com.google.common.truth.Truth;
import org.junit.Test;

import java.util.stream.Collectors;

public class StreamcTest extends TestBase {
    @Test
    public void streamSingle() {
        Truth.assertThat(Streamc.stream(Lists.newArrayList("abc")).collect(Collectors.toSet()))
             .containsExactly("abc");
    }

    @Test
    public void streamMultiple() {
        Truth.assertThat(Streamc.stream(Lists.newArrayList("abc", "123", "xyz")).collect(Collectors.toList()))
             .containsExactly("abc", "123", "xyz");
    }

    @Test
    public void streamEmpty() {
        Truth.assertThat(Streamc.stream(Lists.newArrayList()).collect(Collectors.toSet())).isEmpty();
    }
}
