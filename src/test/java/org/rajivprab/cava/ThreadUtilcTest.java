package org.rajivprab.cava;

import org.junit.Assert;
import org.junit.Test;
import org.rajivprab.cava.exception.CheckedExceptionWrapper;
import org.rajivprab.cava.exception.ExecutionExceptionc;
import org.rajivprab.cava.exception.InterruptedExceptionc;
import org.rajivprab.cava.exception.TimeoutExceptionc;

import java.io.IOException;
import java.time.Duration;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.google.common.truth.Truth.assertThat;

/**
 * Unit tests for ThreadUtil library
 * <p>
 * Created by rprabhakar on 12/22/15.
 */
public class ThreadUtilcTest extends TestBase {
    private static final Duration TEST_DURATION = Duration.ofMillis(2000);
    private static final int RETURN_VALUE = 42;
    private static final Random RNG = new Random();

    // ----------------- sleep ----------------

    @Test
    public void sleep_allGood() {
        checkRunTime(() -> {
            int result = ThreadUtilc.get(runSleepTask(TEST_DURATION));
            assertThat(result).isEqualTo(RETURN_VALUE);
        }, TEST_DURATION);
    }

    // Task-A on Thread-1 is sleeping
    // Task-B on Thread-2 is waiting for Task-A to finish (ie, calling taskA.get)

    // Thread-1 gets interrupted => Task-A will throw InterruptedException => Task-B will throw ExecutionException
    // Thread-2 gets interrupted => Task-B will throw InterruptedException
    // The only way for the main-thread to emit an InterruptedException, is for some other thread to interrupt the main-thread
    @Test
    public void sleep_interrupted_get_throwExecutionException() {
        FutureTask<Integer> taskA = new FutureTask<>(() -> ThreadUtilc.sleep(10000), RETURN_VALUE);
        Thread thread1 = new Thread(taskA);
        thread1.start();
        thread1.interrupt();
        try {
            taskA.get();
            Assert.fail();
        } catch (InterruptedException e) {
            Assert.fail("This will never happen. " +
                                "InterruptedException is converted into ExecutionException for current-thread");
        } catch (ExecutionException e) {
            assertThat(e.getCause()).isInstanceOf(InterruptedExceptionc.class);
            assertThat(((InterruptedExceptionc) e.getCause()).getUnderlying()).isInstanceOf(InterruptedException.class);
        }
    }

    @Test
    public void sleep_interrupted_threadUtilcGet_throwExecException() {
        FutureTask<Integer> taskA = new FutureTask<>(() -> ThreadUtilc.sleep(1000000), RETURN_VALUE);
        Thread thread = new Thread(taskA);
        thread.start();
        thread.interrupt();
        try {
            ThreadUtilc.get(taskA);
            Assert.fail();
        } catch (InterruptedExceptionc e) {
            Assert.fail("This will never happen. " +
                                "InterruptException is converted into ExecException for current-thread");
        } catch (ExecutionExceptionc e) {
            assertThat(e.getUnderlying()).isInstanceOf(ExecutionException.class);
            assertThat(e.getCause()).isInstanceOf(InterruptedExceptionc.class);
            assertThat(((InterruptedExceptionc) e.getCause()).getUnderlying()).isInstanceOf(InterruptedException.class);
        }
    }

    // ------------------------ Get -----------------------

    @Test
    public void call_noErrors() {
        assertThat(ThreadUtilc.call(() -> 1)).isEqualTo(1);
    }

    @Test
    public void call_runTimeException_shouldThrowDirectly() {
        try {
            ThreadUtilc.call(() -> { throw new IllegalStateException("test"); });
            Assert.fail();
        } catch (IllegalStateException e) {
            assertThat(e).hasMessageThat().isEqualTo("test");
        }
    }

    @Test
    public void call_checkedException_shouldThrowWithWrapper() {
        try {
            ThreadUtilc.call(() -> { throw new IOException("test"); });
            Assert.fail();
        } catch (CheckedExceptionWrapper e) {
            assertThat(e.getUnderlying()).isInstanceOf(IOException.class);
            assertThat(e).hasMessageThat().isEqualTo("test");
        }
    }

    // ------------------------ Get -----------------------

    @Test
    public void get_NoTimeout_success() {
        Future<Integer> future = runSleepTask(TEST_DURATION);
        checkRunTime(() -> {
            int result = ThreadUtilc.get(future);
            assertThat(result).isEqualTo(RETURN_VALUE);
        }, TEST_DURATION);
    }

    @Test
    public void get_timeoutSpecifiedButNotHit() {
        Future<Integer> future = runSleepTask(TEST_DURATION);
        checkRunTime(() -> {
            int result = threadUtilcGet(future, TEST_DURATION.multipliedBy(2));
            assertThat(result).isEqualTo(RETURN_VALUE);
        }, TEST_DURATION);
    }

    @Test
    public void get_timeoutHit_shouldThrowTimeException_andReturnAtTimeout() {
        Future<Integer> future = runSleepTask(Duration.ofMinutes(2));
        checkRunTime(() -> {
            try {
                threadUtilcGet(future, TEST_DURATION);
                Assert.fail("Should have hit timeout");
            } catch (TimeoutExceptionc ignored) {
            }
        }, TEST_DURATION);
    }

    @Test
    public void get_executionException_causedByCheckedException_shouldThrowExecException() {
        FutureTask<Integer> future = new FutureTask<>(() -> { throw new IOException("test"); });
        new Thread(future).start();
        try {
            ThreadUtilc.get(future);
            Assert.fail("Exception should have been thrown");
        } catch (ExecutionExceptionc e) {
            assertThat(e.getUnderlying()).isInstanceOf(ExecutionException.class);
            assertThat(e.getUnderlying().getCause()).isInstanceOf(IOException.class);
            assertThat(e.getCause()).isInstanceOf(IOException.class);
            assertThat(e).hasMessageThat().isEqualTo("java.io.IOException: test");
            assertThat(e.toString()).matches(".*ExecutionExceptionc: .*ExecutionException: .*IOException: test");
        }
    }

    @Test
    public void get_executionException_causedByRunTimeException_shouldThrowExecException() {
        FutureTask<Integer> future = new FutureTask<>(() -> { throw new ArithmeticException("test"); });
        new Thread(future).start();
        try {
            ThreadUtilc.get(future);
            Assert.fail("Exception should have been thrown");
        } catch (ExecutionExceptionc e) {
            assertThat(e.getUnderlying()).isInstanceOf(ExecutionException.class);
            assertThat(e.getCause()).isInstanceOf(ArithmeticException.class);
            assertThat(e).hasMessageThat().isEqualTo("java.lang.ArithmeticException: test");
            assertThat(e.toString())
                 .matches(".*ExecutionExceptionc: .*ExecutionException: .*ArithmeticException: test");
        }
    }

    // ---------------- Fork -------------------------

    @Test
    public void forkRunnable() {
        Future<?> task = ThreadUtilc.fork(() -> ThreadUtilc.sleep(TEST_DURATION));
        assertThat(task.isDone()).isFalse();
        checkRunTime(() -> assertThat(ThreadUtilc.get(task)).isNull(),
                     TEST_DURATION.multipliedBy(95).dividedBy(100),
                     TEST_DURATION.multipliedBy(120).dividedBy(100));
    }

    @Test
    public void fork_shouldRunInParallel() {
        checkRunTime(() -> {
            // Getting rid of the intermediate collect will result in all sleeps happening sequentially,
            // because of lazy streams
            List<Integer> result = IntStream.range(0, 5)
                                            .mapToObj(i -> ThreadUtilc.fork(() -> {
                                                ThreadUtilc.sleep(TEST_DURATION);
                                                return i;
                                            }))
                                            .collect(Collectors.toList()).stream()
                                            .map(ThreadUtilc::get)
                                            .collect(Collectors.toList());
            assertThat(result).containsExactly(0, 1, 2, 3, 4).inOrder();
        }, TEST_DURATION, TEST_DURATION.multipliedBy(2));
    }

    // -------------------- Helpers ---------------

    private static void checkRunTime(Runnable runnable, Duration approximate) {
        checkRunTime(runnable,
                     approximate.multipliedBy(98).dividedBy(100), approximate.multipliedBy(120).dividedBy(100));
    }

    private static void checkRunTime(Runnable runnable, Duration min, Duration max) {
        long controlStart = System.nanoTime();
        long start = System.nanoTime();
        runnable.run();
        long end = System.nanoTime();
        Duration duration = Duration.ofNanos((end - start) - (start - controlStart));

        assertThat(duration).isAtLeast(min);
        assertThat(duration).isLessThan(max);
    }

    private static FutureTask<Integer> runSleepTask(Duration sleep) {
        boolean useDuration = RNG.nextBoolean();
        FutureTask<Integer> futureTask = new FutureTask<>(() -> {
            if (useDuration) {
                ThreadUtilc.sleep(sleep);
            } else {
                ThreadUtilc.sleep(sleep.toMillis());
            }
        }, RETURN_VALUE);
        new Thread(futureTask).start();
        return futureTask;
    }

    private static <T> T threadUtilcGet(Future<T> future, Duration duration) {
        return RNG.nextBoolean() ? ThreadUtilc.get(future, duration) : ThreadUtilc.get(future, duration.toMillis());
    }
}
