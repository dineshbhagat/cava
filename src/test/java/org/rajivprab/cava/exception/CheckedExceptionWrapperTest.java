package org.rajivprab.cava.exception;

import org.junit.Test;
import org.rajivprab.cava.TestBase;

// TODO Write tests
public class CheckedExceptionWrapperTest extends TestBase {
    @Test
    public void wrapIfNeeded_shouldWrapCheckedExceptions() {

    }

    @Test
    public void wrapIfNeeded_shouldNotWrapRunTimeExceptions() {

    }

    @Test
    public void wrap_shouldWrapCheckedExceptions() {

    }

    @Test
    public void wrap_shouldWrapRunTimeExceptions() {

    }

    @Test
    public void shouldHaveSameStackTraceAsUnderlying() {

    }

    @Test
    public void shouldHaveSameMessageAsUnderlying() {

    }

    @Test
    public void shouldHaveSameDetailedMessageAsUnderlying() {

    }

    @Test
    public void shouldHaveSameCauseAsUnderlying() {

    }

    @Test
    public void toString_shouldPrependClassName() {

    }
}
