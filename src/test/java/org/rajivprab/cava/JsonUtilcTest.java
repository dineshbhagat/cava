package org.rajivprab.cava;

import com.google.common.collect.Lists;
import com.google.common.truth.Truth;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import static org.junit.Assert.assertTrue;

/**
 * Unit tests for DataUtil methods
 * <p>
 * Created by rprabhakar on 7/29/15.
 */
public class JsonUtilcTest extends TestBase {
    @Test
    public void mergeAllObjectsIntoArray() {
        JSONArray jsonArray = JsonUtilc.mergeAllObjectsIntoArray(Lists.newArrayList("one", "two", "three"));
        Truth.assertThat(JsonUtilc.getIterable(jsonArray)).containsExactly("one", "two", "three");
    }

    @Test
    public void concurrentModification_shouldThrowException() {
        JSONArray array = new JSONArray().put(1);
        Iterator<String> iterator = JsonUtilc.<String>getIterable(array).iterator();
        Truth.assertThat(iterator.hasNext()).isTrue();
        array.put(2);
        try {
            iterator.hasNext();
            Assert.fail("Expecting exception to be thrown");
        } catch (ConcurrentModificationException ignored) {}
    }

    @Test
    public void emptyIterable_callingNext_shouldProduceError() {
        Iterable<String> iterable = JsonUtilc.getIterable(new JSONArray());
        Truth.assertThat(iterable).isEmpty();
        Truth.assertThat(iterable.iterator().hasNext()).isFalse();
        try {
            iterable.iterator().next();
            Assert.fail("Expecting exception to be thrown");
        } catch (NoSuchElementException ignored) {}
    }

    @Test
    public void arrayStreamInt() {
        JSONArray array = new JSONArray().put(1).put(2).put(3);
        Truth.assertThat(JsonUtilc.<Integer>getStream(array).collect(Collectors.toList()))
             .containsExactly(1, 2, 3).inOrder();
    }

    @Test
    public void arrayIterableInt() {
        JSONArray array = new JSONArray().put(1).put(2).put(3);
        Iterable<Integer> iterable = JsonUtilc.getIterable(array);
        Truth.assertThat(iterable).containsExactly(1, 2, 3).inOrder();
    }

    @Test
    public void arrayIterableString() {
        JSONArray array = new JSONArray().put("a").put("b").put("c");
        Iterable<String> iterable = JsonUtilc.getIterable(array);
        Truth.assertThat(iterable).containsExactly("a", "b", "c").inOrder();
    }

    @Test
    public void arrayIterableJsonObject() {
        JSONObject entry1 = new JSONObject(Mapc.newHashMap("a", 1));
        JSONObject entry2 = new JSONObject(Mapc.newHashMap("b", 2));
        JSONArray array = new JSONArray().put(entry1).put(entry2);
        Iterable<JSONObject> iterable = JsonUtilc.getIterable(array);
        Truth.assertThat(iterable).containsExactly(entry1, entry2).inOrder();
    }

    @Test
    public void arrayInteger_expectString_runsFineIfNotReadToTypedVar() {
        JSONArray array = new JSONArray().put(1).put(2).put(3);
        Iterable<String> iterable = JsonUtilc.getIterable(array);
        Truth.assertThat(iterable).containsExactly(1, 2, 3).inOrder();
    }

    @Test (expected = ClassCastException.class)
    public void arrayInteger_expectString_throwsErrorWhenRead() {
        JSONArray array = new JSONArray().put(1).put(2).put(3);
        Iterable<String> iterable = JsonUtilc.getIterable(array);
        Truth.assertThat(iterable).containsExactly(1, 2, 3).inOrder();
        String num = iterable.iterator().next();
        log.info(num);
    }

    @Test (expected = ClassCastException.class)
    public void arrayIterableString_expectInt_shouldThrowError() {
        JSONArray array = new JSONArray().put("a").put("b").put("c");
        Iterator<Integer> iterable = JsonUtilc.<Integer>getIterable(array).iterator();  // Does not produce any error
        Integer num = iterable.next();                                                  // Produces exception
        log.info(num);
    }

    @Test (expected = ClassCastException.class)
    public void arrayIterableString_expectJson_shouldThrowError() {
        JSONArray array = new JSONArray().put("a").put("b").put("c");
        Iterator<JSONObject> iterable = JsonUtilc.<JSONObject>getIterable(array).iterator();  // Does not produce any error
        JSONObject entry = iterable.next();                                                   // Produces exception
        log.info(entry);
    }

    @Test
    public void shouldEqual_jsonObject() {
        assertTrue(JsonUtilc.equals(getTestJson(), getTestJson()));
    }

    @Test
    public void shouldEqual_string() {
        assertTrue(JsonUtilc.equals("abc", "abc"));
    }

    @Test
    public void shouldEqual_jsonArray() {
        assertTrue(JsonUtilc.equals(getTestArray(), getTestArray()));
    }

    @Test
    public void shouldEqual_jsonArray2() {
        assertTrue(JsonUtilc.equals(getTestArray2(), getTestArray2()));
    }

    @Test
    public void shouldNotEqual_jsonObject() {
        Assert.assertFalse(JsonUtilc.equals(getTestJson(), getTestJson2()));
    }

    @Test
    public void shouldNotEqual_jsonArray() {
        Assert.assertFalse(JsonUtilc.equals(getTestArray(), getTestArray2()));
    }

    @Test
    public void shouldNotEqual_jsonArray2() {
        Assert.assertFalse(JsonUtilc.equals(getTestArray(), getTestArray3()));
    }

    @Test
    public void shouldNotEqual_string() {
        Assert.assertFalse(JsonUtilc.equals("", "a"));
    }

    @Test
    public void shouldNotEqual_objectArray() {
        Assert.assertFalse(JsonUtilc.equals(getTestJson(), getTestArray()));
    }

    @Test
    public void shouldNotEqual_arrayString() {
        Assert.assertFalse(JsonUtilc.equals(getTestArray3(), "abc"));
    }

    // ---------- Helpers ----------

    private static JSONObject getTestJson() {
        return new JSONObject(
                "{\"records\":[[\"hello\",\"world\",100000,\"Indus Valley\"]],\"fields\":[{\"name\":\"theme\"," +
                        "\"type\":\"VARCHAR\"},{\"name\":\"name\",\"type\":\"VARCHAR\"}," +
                        "{\"name\":\"aggregate_reputation\",\"type\":\"DOUBLE\"},{\"name\":\"constitution\"," +
                        "\"type\":\"CLOB\"}]}");
    }

    private static JSONObject getTestJson2() {
        return new JSONObject(
                "{\"records\":[[\"hello\",\"testname\",100000,\"Mesoptamaei\"]],\"fields\":[{\"name\":\"theme\"," +
                        "\"type\":\"VARCHAR\"},{\"name\":\"name\",\"type\":\"VARCHAR\"}," +
                        "{\"name\":\"aggregate_reputation\",\"type\":\"DOUBLE\"},{\"name\":\"constitution\"," +
                        "\"type\":\"CLOB\"}]}");
    }

    private static JSONArray getTestArray() {
        return getTestJson().getJSONArray("fields");
    }

    private static JSONArray getTestArray2() {
        return getTestJson().getJSONArray("records");
    }

    private static JSONArray getTestArray3() {
        return getTestJson2().getJSONArray("records");
    }
}
