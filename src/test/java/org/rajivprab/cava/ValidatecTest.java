package org.rajivprab.cava;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.time.Duration;
import java.time.Instant;
import java.util.*;

import static com.google.common.truth.Truth.assertThat;

/**
 * Unit tests for Validatec
 * <p>
 * Created by rprabhakar on 12/15/15.
 */
public class ValidatecTest extends TestBase {

    // ------------ IsTrue ------------

    @Test
    public void isTrue_allGood() {
        Validatec.isTrue(true);
    }

    @Test(expected = IllegalArgumentException.class)
    public void isNotTrue_throwDefaultException() {
        Validatec.isTrue(false);
    }

    @Test(expected = ArithmeticException.class)
    public void isNotTrue_throwGivenException() {
        Validatec.isTrue(false, ArithmeticException::new);
    }

    @Test
    public void isNotTrue_constructException_withMessage() {
        try {
            Validatec.isTrue(false, ArithmeticException.class, "1 is not equal to 2");
            Assert.fail();
        } catch (ArithmeticException e) {
            assertThat(e).hasMessageThat().contains("1 is not equal to 2");
        }
    }

    @Test
    public void isNotTrue_constructDefaultException_withMessage() {
        try {
            Validatec.isTrue(false, "my custom message");
            Assert.fail();
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageThat().contains("my custom message");
        }
    }

    @Test
    public void isTrue_supplierNotInvoked() {
        Validatec.isTrue(true, () -> { throw new IllegalStateException("Supplier should not be called"); });
    }

    // ------------ IsFalse ------------

    @Test
    public void isFalse_allGood() {
        Validatec.isFalse(false);
    }

    @Test(expected = IllegalArgumentException.class)
    public void isFalse_failed_throwDefaultException() {
        Validatec.isFalse(true);
    }

    @Test(expected = ArithmeticException.class)
    public void isFalse_failed_throwGivenException() {
        Validatec.isFalse(true, ArithmeticException::new);
    }

    @Test
    public void isFalse_failed_constructCustomException() {
        try {
            Validatec.isFalse(true, ArithmeticException.class, "2 is equal to 2");
            Assert.fail();
        } catch (ArithmeticException e) {
            assertThat(e).hasMessageThat().contains("2 is equal to 2");
        }
    }

    @Test
    public void isFalse_failed_constructCustomException_ofDefaultType() {
        try {
            Validatec.isFalse(true, "2 is equal to 2");
            Assert.fail();
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageThat().contains("2 is equal to 2");
        }
    }

    // ------------ IsNull ------------

    @Test
    public void isNull_allGood() {
        Validatec.isNull(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void isNull_fail_constructDefaultException() {
        Validatec.isNull(1);
    }

    @Test(expected = ArithmeticException.class)
    public void isNull_fail_throwGivenException() {
        Validatec.isNull("", ArithmeticException::new);
    }

    @Test
    public void isNull_fail_constructException() {
        try {
            Validatec.isNull(true, ArithmeticException.class, "Expecting to find null");
            Assert.fail();
        } catch (ArithmeticException e) {
            assertThat(e).hasMessageThat().contains("Expecting to find null");
        }
    }

    @Test
    public void isNull_fail_constructWithGivenMessage() {
        try {
            Validatec.isNull(true, "my msg");
            Assert.fail();
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageThat().contains("my msg");
        }
    }

    // ------------ NotNull ------------

    @Test
    public void notNull_allGood_primitive() {
        assertThat(Validatec.notNull(123)).isEqualTo(123);
    }

    @Test
    public void notNull_allGood_object() {
        assertThat(Validatec.notNull("")).isEqualTo("");
    }

    @Test(expected = IllegalArgumentException.class)
    public void notNull_fail_constructDefaultException() {
        Validatec.notNull(null);
    }

    @Test(expected = ArithmeticException.class)
    public void notNull_fail_throwGivenException() {
        Validatec.notNull(null, ArithmeticException::new);
    }

    @Test
    public void notNull_fail_constructException() {
        try {
            Validatec.notNull(null, ArithmeticException.class);
            Assert.fail();
        } catch (ArithmeticException expected) {
            assertThat(expected).hasMessageThat().isEqualTo("Argument should not be null");
        }
    }

    // ------------ NoNulls - varargs ------------

    @Test
    public void noNulls_allGood() {
        Validatec.noNulls(123, "abc");
    }

    @Test
    public void noNulls_allGood_shouldNotCallToString_forPerformanceReasons() {
        Iterator element = Mockito.mock(Iterator.class);
        Mockito.when(element.toString()).thenThrow(new UnsupportedOperationException());
        Validatec.noNulls(element);
    }

    @Test
    public void noNulls_fail_throwGivenException() {
        try {
            Validatec.noNulls(() -> new ArithmeticException("abc"), null, Optional.empty());
            Assert.fail();
        } catch (ArithmeticException e) {
            assertThat(e).hasMessageThat().isEqualTo("abc");
        }
    }

    @Test
    public void noNulls_nullArray_constructDefaultException() {
        try {
            Validatec.noNulls((Object) null, Optional.empty());
            Assert.fail();
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageThat().isEqualTo("[null, Optional.empty] should not contain any nulls");
        }
    }

    // ------------ NoNullsIn ------------

    @Test
    public void noNullsIn_allGood() {
        Validatec.noNullsIn(Lists.newArrayList(123, "abc"));
    }

    @Test
    public void noNullsIn_fail_throwGivenException() {
        try {
            Validatec.noNullsIn(Lists.newArrayList(null, Optional.empty()), ArithmeticException.class);
            Assert.fail();
        } catch (ArithmeticException e) {
            assertThat(e).hasMessageThat().isEqualTo("[null, Optional.empty] should not contain any nulls");
        }
    }

    @Test
    public void noNullsIn_fail_throwGivenException_withGivenMessage() {
        try {
            Validatec.noNullsIn(Lists.newArrayList(null, Optional.empty()), ArithmeticException.class, "custom");
            Assert.fail();
        } catch (ArithmeticException e) {
            assertThat(e).hasMessageThat().isEqualTo("custom");
        }
    }

    @Test
    public void noNullsIn_nullArray_constructDefaultException_withGivenMessage() {
        try {
            Validatec.noNullsIn(Lists.newArrayList(null, Optional.empty()), "custom msg");
            Assert.fail();
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageThat().isEqualTo("custom msg");
        }
    }

    // ------------ Collection.Size ------------

    @Test
    public void matchesSize_allGood() {
        Validatec.size(ImmutableList.of(1, 2), 2);
    }

    @Test
    public void matchesSize_successful_toStringShouldNotBeCalled_forPerformanceReasons() {
        List<Integer> spyList = Mockito.spy(ImmutableList.of(1, 2));
        Mockito.when(spyList.toString()).thenThrow(new UnsupportedOperationException());
        Validatec.size(spyList, 2);
    }

    @Test
    public void doesNotMatchSize_throwGivenExceptionType() {
        try {
            Validatec.size(ImmutableList.of(1, 2), 3, ArithmeticException.class);
            Assert.fail();
        } catch (ArithmeticException e) {
            assertThat(e).hasMessageThat().isEqualTo("[1, 2] should have 3 entries");
        }
    }

    @Test
    public void doesNotMatchSize_throwDefaultException_customMessage() {
        try {
            Validatec.size(ImmutableList.of(1, 2), 3, "my custom msg");
            Assert.fail();
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageThat().isEqualTo("my custom msg");
        }
    }

    @Test
    public void nullCollection_throwExceptionGiven() {
        try {
            Validatec.size(null, 3, ArithmeticException.class);
            Assert.fail();
        } catch (ArithmeticException e) {
            assertThat(e).hasMessageThat().isEqualTo("null should have 3 entries");
        }
    }

    // ------------ Array.length ------------

    @Test
    public void matchesArrayLength_allGood() {
        Validatec.length(new Integer[]{1, 2}, 2);
    }

    @Test
    public void matchesArrayLength_allGood_shouldNotCallToString() {
        Object element = Mockito.mock(Object.class);
        Mockito.when(element.toString()).thenThrow(new UnsupportedOperationException());
        Validatec.length(new Object[]{element}, 1);
    }

    @Test
    public void doesNotMatchArrayLength_throwGivenExceptionType() {
        try {
            Validatec.length(new Character[]{'a', 'b'}, 3, ArithmeticException.class);
            Assert.fail();
        } catch (ArithmeticException e) {
            assertThat(e).hasMessageThat().isEqualTo("[a, b] should have 3 entries");
        }
    }

    @Test
    public void doesNotMatchArrayLength_throwDefaultException_withCustomMessage() {
        try {
            Validatec.length(new Character[]{'a', 'b'}, 3, "random msg 2");
            Assert.fail();
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageThat().isEqualTo("random msg 2");
        }
    }

    @Test
    public void nullArray_throwExceptionGiven() {
        try {
            Validatec.length((Object[]) null, 3, ArithmeticException.class);
            Assert.fail();
        } catch (ArithmeticException e) {
            assertThat(e).hasMessageThat().isEqualTo("null should have 3 entries");
        }
    }

    // ------------ String.length ------------

    @Test
    public void matchesStringLength_allGood() {
        Validatec.length("12", 2);
    }

    @Test
    public void doesNotMatchStringLength_throwGivenExceptionType() {
        try {
            Validatec.length("ab", 3, ArithmeticException.class);
            Assert.fail();
        } catch (ArithmeticException e) {
            assertThat(e).hasMessageThat().isEqualTo("'ab' should be 3 characters long");
        }
    }

    @Test
    public void doesNotMatchStringLength_throwGivenExceptionMessage() {
        try {
            Validatec.length("ab", 3, "random msg 3");
            Assert.fail();
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageThat().isEqualTo("random msg 3");
        }
    }

    @Test
    public void nullString_throwExceptionGiven() {
        try {
            Validatec.length((String) null, 4, ArithmeticException.class);
            Assert.fail();
        } catch (ArithmeticException e) {
            assertThat(e).hasMessageThat().contains("null should be 4 characters long");
        }
    }

    // ------------ String.matches ------------

    @Test
    public void matchesString_allGood() {
        Validatec.matches("abc", "[cab]+");
    }

    @Test
    public void doesNotMatchString_throwGivenExceptionType() {
        try {
            Validatec.matches("abc", "[ab]+", ArithmeticException.class);
            Assert.fail();
        } catch (ArithmeticException e) {
            assertThat(e).hasMessageThat().isEqualTo("'abc' should match '[ab]+'");
        }
    }

    @Test
    public void nullString_failMatch_throwExceptionMessage() {
        try {
            Validatec.matches(null, "[abc]+", "random msg 3");
            Assert.fail();
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageThat().isEqualTo("random msg 3");
        }
    }

    @Test
    public void nullRegex_failMatch_throwExceptionGiven() {
        try {
            Validatec.matches("abc", null, ArithmeticException.class);
            Assert.fail();
        } catch (ArithmeticException e) {
            assertThat(e).hasMessageThat().isEqualTo("'abc' should match null");
        }
    }

    // ------------ Equals ------------

    @Test
    public void equalsMatch_allGood() {
        Validatec.equals("test", "test", IllegalStateException.class);
    }

    @Test
    public void equalsIsTrue_nullExceptionGiven_shouldPass() {
        Validatec.equals("test", "test", () -> null);
    }

    @Test(expected = NullPointerException.class)
    public void equalsIsFalse_nullExceptionGiven_shouldThrowNullPointerException() {
        Validatec.equals("test1", "test2", () -> null);
    }

    @Test
    public void equalsIsFalse_nullMessageGiven_shouldThrowGivenExceptionType() {
        try {
            Validatec.equals("test1", "test2", ArithmeticException.class, null);
            Assert.fail();
        } catch (ArithmeticException e) {
            assertThat(e).hasMessageThat().isEmpty();
        }
    }

    @Test
    public void equalsIsFalse_nullMessageGiven_shouldThrowGivenExceptionMessage() {
        try {
            Validatec.equals("test1", "test2", "random msg 3");
            Assert.fail();
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageThat().isEqualTo("random msg 3");
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void equalsOneNull_throwDefaultException() {
        Validatec.equals(null, "test");
    }

    @Test(expected = IllegalStateException.class)
    public void equalsTwoNull_throwGivenException() {
        Validatec.equals("test", null, IllegalStateException::new);
    }

    @Test(expected = IllegalStateException.class)
    public void equalsBothNull_constructGivenException() {
        Validatec.equals(null, null, IllegalStateException.class);
    }

    @Test(expected = IllegalStateException.class)
    public void equalsMismatch_throwGivenException() {
        Validatec.equals("teest", "test", IllegalStateException::new);
    }

    // ------------ Not equals ------------

    @Test(expected = IllegalArgumentException.class)
    public void notEqualsOneNull() {
        Validatec.notEquals(null, "test");
    }

    @Test(expected = IllegalStateException.class)
    public void notEqualsTwoNull() {
        Validatec.notEquals("test", null, IllegalStateException::new);
    }

    @Test(expected = IllegalStateException.class)
    public void notEqualsBothNull() {
        Validatec.notEquals(null, null, IllegalStateException.class);
    }

    @Test
    public void notEqualsMismatch() {
        Validatec.notEquals("teest", "test", IllegalStateException::new);
    }

    @Test(expected = IllegalStateException.class)
    public void notEqualsMatch_throwGivenException() {
        Validatec.notEquals("test", "test", IllegalStateException.class);
    }

    @Test
    public void notEqualsMatch_throwGivenMessage() {
        try {
            Validatec.notEquals("test", "test", "random msg 5");
            Assert.fail();
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageThat().isEqualTo("random msg 5");
        }
    }

    // ------------ Greater than ------------

    @Test
    public void greaterThanSucceedsWithInt() {
        Validatec.greaterThan(2, 1);
    }

    @Test
    public void greaterThanSucceedsWithDouble() {
        Validatec.greaterThan(2.0, 1.9999999);
    }

    @Test(expected = IllegalArgumentException.class)
    public void greaterThanFails_throwsDefaultException() {
        Validatec.greaterThan(2.0, 2.0);
    }

    @Test
    public void greaterThanFails_throwsExceptionWithCustomMessage() {
        try {
            Validatec.greaterThan(2.0, 2.0, "abc");
            Assert.fail();
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageThat().isEqualTo("abc");
        }
    }

    @Test(expected = ArithmeticException.class)
    public void greaterThanFails_throwsSpecifiedException() {
        Validatec.greaterThan(2.0, 2.0, ArithmeticException::new);
    }

    // ------------ Greater or Equal --------

    @Test
    public void greaterOrEqualSucceedsWithInt_greater() {
        Validatec.greaterOrEqual(2, 1);
    }

    @Test
    public void greaterOrEqualSucceedsWithDouble_equal() {
        Validatec.greaterOrEqual(2.0, 2.0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void greaterOrEqualFails_throwsDefaultException() {
        Validatec.greaterOrEqual(2.0, 2.01);
    }

    @Test
    public void greaterOrEqualFails_throwsExceptionWithCustomMessage() {
        try {
            Validatec.greaterOrEqual(Instant.now(), Instant.now().plusSeconds(1), "abc");
            Assert.fail();
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageThat().isEqualTo("abc");
        }
    }

    @Test(expected = ArithmeticException.class)
    public void greaterOrEqualFails_throwsSpecifiedException() {
        Validatec.greaterOrEqual(Duration.ofMillis(99), Duration.ofMillis(100), ArithmeticException::new);
    }

    // ------------ String contains ------------

    @Test
    public void stringContains_allGood() {
        Validatec.contains("testing", "ing");
    }

    @Test
    public void stringContains_fails_throwsDefaultException() {
        try {
            Validatec.contains("testing", "sing");
            Assert.fail();
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageThat().isEqualTo("'testing' should contain 'sing'");
        }
    }

    @Test
    public void stringContains_fails_throwsWithGivenMessage() {
        try {
            Validatec.contains("testing", "sing", "my random msg");
            Assert.fail();
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageThat().isEqualTo("my random msg");
        }
    }

    // ------------ Collection notEmpty ------------

    @Test
    public void collectionIsNotEmpty_allGood() {
        Validatec.notEmpty(Lists.newArrayList(1));
    }

    @Test
    public void collectionIsNotEmpty_allGood_shouldNotCallToString() {
        List<String> spyList = Mockito.spy(Lists.newArrayList("abc"));
        Mockito.when(spyList.toString()).thenThrow(new UnsupportedOperationException());
        Validatec.notEmpty(spyList);
    }

    @Test
    public void collectionIsEmpty_constructDefaultException() {
        try {
            Validatec.notEmpty(Lists.newArrayList());
            Assert.fail();
        } catch (IllegalArgumentException expected) {
            assertThat(expected).hasMessageThat().isEqualTo("[] should not be empty");
        }
    }

    @Test
    public void collectionIsEmpty_customMessage() {
        try {
            Validatec.notEmpty(Lists.newArrayList(), "abc");
            Assert.fail();
        } catch (IllegalArgumentException expected) {
            assertThat(expected).hasMessageThat().isEqualTo("abc");
        }
    }

    // ------------ String notEmpty ------------

    @Test
    public void stringNotEmpty_allGood() {
        assertThat(Validatec.notEmpty("a", ArithmeticException.class, null)).isEqualTo("a");
    }

    @Test
    public void stringNotEmpty_nullMessage_allGood() {
        assertThat(Validatec.notEmpty("random")).isEqualTo("random");
    }

    @Test
    public void stringNotEmpty_customMessage_allGood() {
        assertThat(Validatec.notEmpty("input", "my error message")).isEqualTo("input");
    }

    @Test
    public void stringNotEmpty_failEmpty_constructDefaultException() {
        try {
            Validatec.notEmpty("");
            Assert.fail();
        } catch (IllegalArgumentException expected) {
            assertThat(expected).hasMessageThat()
                                .isEqualTo("'' should not be null or empty");
        }
    }

    @Test
    public void stringNotEmpty_failEmpty_constructDefaultException_withGivenMessage() {
        try {
            Validatec.notEmpty("", "my custom msg");
            Assert.fail();
        } catch (IllegalArgumentException expected) {
            assertThat(expected).hasMessageThat().isEqualTo("my custom msg");
        }
    }

    @Test
    public void stringNotEmpty_failNull_constructGivenException() {
        try {
            Validatec.notEmpty((String) null, ArithmeticException.class, "my custom message");
            Assert.fail();
        } catch (ArithmeticException expected) {
            assertThat(expected).hasMessageThat().contains("my custom message");
        }
    }

    // ------------ Strings[] notEmpty ------------

    @Test
    public void stringsNoneEmpty_allGood() {
        Validatec.noneEmpty("a", "b", "c");
    }

    @Test
    public void stringsNoneEmpty_containsEmpty_constructDefaultException() {
        try {
            Validatec.noneEmpty("123", "");
            Assert.fail();
        } catch (IllegalArgumentException expected) {
            assertThat(expected).hasMessageThat()
                                .isEqualTo("[123, ] should not be empty, and should not contain null/empty strings");
        }
    }

    @Test
    public void stringsNoneEmpty_containsNull_constructGivenException() {
        try {
            Validatec.noneEmpty(() -> new ArithmeticException("my custom message"), null, "123");
            Assert.fail();
        } catch (ArithmeticException expected) {
            assertThat(expected).hasMessageThat().contains("my custom message");
        }
    }

    @Test
    public void stringsNoneEmpty_arrayIsNull_constructGivenException() {
        try {
            Validatec.noneEmpty(() -> new ArithmeticException("my custom message"), (String[]) null);
            Assert.fail();
        } catch (ArithmeticException expected) {
            assertThat(expected).hasMessageThat().contains("my custom message");
        }
    }

    // ------------ Iterable<Strings> notEmpty ------------

    @Test
    public void stringsNoneEmpty_iterable_allGood() {
        Validatec.noneEmpty(Lists.newArrayList("a", "b", "c"));
    }

    @Test
    public void stringsNoneEmpty_iterable_allGood_shouldNotCallToString() {
        List<String> spyList = Mockito.spy(Lists.newArrayList("elem"));
        Mockito.when(spyList.toString()).thenThrow(new UnsupportedOperationException());
        Validatec.noneEmpty(spyList);
    }

    @Test
    public void stringsNoneEmpty_iterable_containsEmpty_constructDefaultException() {
        try {
            Validatec.noneEmpty(Lists.newArrayList("123", ""));
            Assert.fail();
        } catch (IllegalArgumentException expected) {
            assertThat(expected).hasMessageThat().isEqualTo("[123, ] should not contain null/empty strings");
        }
    }

    @Test
    public void stringsNoneEmpty_iterable_containsNull_constructGivenException() {
        try {
            Validatec.noneEmpty(Lists.newArrayList(null, "123"), ArithmeticException.class, "custom message");
            Assert.fail();
        } catch (ArithmeticException expected) {
            assertThat(expected).hasMessageThat().contains("custom message");
        }
    }

    @Test
    public void stringsNoneEmpty_iterableIsNull_constructGivenException() {
        try {
            Validatec.noneEmpty(null, () -> new ArithmeticException("my custom message"));
            Assert.fail();
        } catch (ArithmeticException expected) {
            assertThat(expected).hasMessageThat().contains("my custom message");
        }
    }

    // ------------------------ Map contains ------------------------

    @Test
    public void mapDoesContain_allGood() {
        Validatec.contains(ImmutableMap.of(1, "one", 3, "three"), 1, "one");
    }

    @Test
    public void mapDoesContain_allGood_doesNotGenerateErrorMessage() {
        Map<Integer, String> map = Mockito.spy(Mapc.newHashMap(1, "one", 3, "three"));
        Mockito.when(map.entrySet()).thenThrow(new UnsupportedOperationException());
        Validatec.contains(map, 1, "one");
    }

    @Test
    public void mapDoesNotContainValue_constructGivenException() {
        try {
            Validatec.contains(ImmutableMap.of(1, "one", 3, "three"), 3, "one", ArithmeticException.class);
            Assert.fail();
        } catch (ArithmeticException e) {
            assertThat(e).hasMessageThat().contains("[1=one, 3=three]");
            assertThat(e).hasMessageThat().contains("should contain: 3=one");
        }
    }

    @Test
    public void mapDoesNotContainKey_constructGivenException() {
        try {
            Validatec.contains(ImmutableMap.of(1, "one", 3, "three"), 4, "one", ArithmeticException.class);
            Assert.fail();
        } catch (ArithmeticException e) {
            assertThat(e).hasMessageThat().contains("[1=one, 3=three]");
            assertThat(e).hasMessageThat().contains("should contain: 4=one");
        }
    }

    @Test
    public void mapDoesNotContain_constructDefaultException_withCustomMessage() {
        try {
            Validatec.contains(ImmutableMap.of(1, "one", 3, "three"), 1, "three", "my error message");
            Assert.fail();
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageThat().contains("my error message");
        }
    }

    // ------------------------ Collection contains ------------------------

    @Test
    public void collectionDoesContain_allGood_doesNotCallToString() {
        List<Integer> spyList = Mockito.spy(Lists.newArrayList(0, 1, 2, 3));
        Mockito.when(spyList.toString()).thenThrow(new UnsupportedOperationException());
        Validatec.contains(spyList, new Random().nextInt(4));
    }

    @Test(expected = ArithmeticException.class)
    public void collectionDoesNotContain_throwGivenException() {
        Validatec.contains(Lists.newArrayList(1, 2, 3), 4, ArithmeticException::new);
    }

    @Test(expected = ArithmeticException.class)
    public void collectionDoesNotContain_constructSpecifiedException() {
        Validatec.contains(Lists.newArrayList(1, 2, 3), 4, ArithmeticException.class);
    }

    @Test
    public void collectionDoesNotContain_throwsWithGivenMessage() {
        try {
            Validatec.contains(Lists.newArrayList(1, 2, 3), 4, "my random msg");
            Assert.fail();
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageThat().isEqualTo("my random msg");
        }
    }

    // ------------------ Collection does not contain ---------------------

    @Test
    public void collectionDoesNotContain_allGood_doesNotGenerateErrorString() {
        List<Integer> spyList = Mockito.spy(Lists.newArrayList(1, 2, 3));
        Mockito.when(spyList.toString()).thenThrow(new UnsupportedOperationException());
        Validatec.doesNotContain(spyList, 7);
    }

    @Test(expected = ArithmeticException.class)
    public void collectionDoesContain_constructSpecifiedException() {
        Validatec.doesNotContain(Lists.newArrayList(0, 1, 2, 3), new Random().nextInt(4),
                                 ArithmeticException.class);
    }

    @Test
    public void collectionDoesContain_constructDefaultException_withMessage() {
        try {
            Validatec.doesNotContain(Lists.newArrayList(1, 2, 3), 2, "abc");
            Assert.fail();
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageThat().isEqualTo("abc");
        }
    }

    @Test(expected = ArithmeticException.class)
    public void collectionDoesContain_throwGivenException() {
        Validatec.doesNotContain(Lists.newArrayList(1, 2, 3), 2, ArithmeticException::new);
    }

    // ------------ Private utils ------------

    @Test
    public void constructsRightException() {
        try {
            Validatec.isTrue(false, IllegalThreadStateException.class, "testing");
        } catch (IllegalThreadStateException e) {
            assertThat(e).hasMessageThat().contains("testing");
        }
    }

    @Test
    public void throwsRightException() {
        try {
            Validatec.isTrue(false, () -> new IllegalThreadStateException("testing"));
        } catch (IllegalThreadStateException e) {
            assertThat(e).hasMessageThat().contains("testing");
        }
    }

    @Test
    public void parentCatchWorks() {
        try {
            Validatec.isTrue(false, IllegalThreadStateException.class, "testing");
        } catch (RuntimeException e) {
            assertThat(e).hasMessageThat().contains("testing");
        }
    }
}
