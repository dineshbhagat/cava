package org.rajivprab.cava;

import com.google.common.truth.Truth;
import org.junit.Test;

public class WebUtilcTest extends TestBase {
    @Test
    public void getHtmlLink() {
        Truth.assertThat(WebUtilc.getHtmlLink("caucus", "www.thecaucus.net"))
             .isEqualTo("<a href=\"www.thecaucus.net\" target=\"_blank\">caucus</a>");
    }
}
