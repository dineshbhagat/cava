package org.rajivprab.cava;

import com.google.common.truth.Truth;
import org.junit.Assert;
import org.junit.Test;
import org.rajivprab.cava.exception.IOExceptionc;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

/**
 * Unit tests for FileUtilc
 *
 * Created by rajivprab on 5/18/17.
 */
public class FileUtilcTest extends TestBase {
    @Test
    public void copyToFile() {
        // TODO Copy all tests from ellie
    }

    @Test
    public void readResourceFileFromMain() {
        Truth.assertThat(FileUtilc.readClasspathFile("sample_file.txt"))
             .isEqualTo("sample file\nline 2\nline 3");
    }

    @Test
    public void readResourceFileFromTest() {
        Truth.assertThat(FileUtilc.readClasspathFile("test_file.txt")).isEqualTo("test file");
    }

    @Test
    public void classPathFileNotFound() {
        try {
            FileUtilc.readClasspathFile("not_found.txt");
            Assert.fail();
        } catch (IOExceptionc e) {
            Truth.assertThat(e).hasMessageThat().contains("Classpath file does not exist: not_found.txt");
        }
    }

    @Test
    public void readSystemFile() {
        File file = new File("/tmp/tmp-cava-readSystemFile.txt");
        file.deleteOnExit();
        try (Writer writer = new FileWriter(file)) {
            writer.write("readSystemFile test");
        } catch (IOException e) {
            throw new IOExceptionc(e);
        }
        Truth.assertThat(FileUtilc.readFileToString(file)).contains("readSystemFile test");
    }

    @Test
    public void readSystemFile_doesNotExist() {
        try {
            FileUtilc.readFileToString(new File("/var/tmp/does-not-exist"));
            Assert.fail();
        } catch (IOExceptionc e) {
            Truth.assertThat(e).hasMessageThat().contains("File '/var/tmp/does-not-exist' does not exist");
        }
    }
}
