package org.rajivprab.cava;

import com.google.common.collect.ImmutableList;
import org.junit.Assert;
import org.junit.Test;
import org.rajivprab.cava.exception.InterruptedExceptionc;

import java.util.NoSuchElementException;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.google.common.truth.Truth.assertThat;

public class DynamicConstantTest extends TestBase {
    @Test
    public void lazyDefaultShouldNeverGetCalled_ifSetIsInvoked() {
        DynamicConstant<String> dynamicConstant = DynamicConstant.lazyDefault(() -> LandmineHolder.VAL);
        assertThat(dynamicConstant.isSet()).isFalse();
        assertThat(dynamicConstant.isSet()).isFalse();
        dynamicConstant.set("override");
        assertThat(dynamicConstant.isSet()).isTrue();
        assertThat(dynamicConstant.get()).isEqualTo("override");
    }

    @Test
    public void lazyDefaultShouldBeUsed_multipleTimes_ifSetIsNotInvoked() {
        DynamicConstant<String> dynamicConstant = DynamicConstant.lazyDefault(() -> SafeHolder.VAL);
        assertThat(dynamicConstant.isSet()).isFalse();
        assertThat(dynamicConstant.get()).isEqualTo(SafeHolder.VAL);
        assertThat(dynamicConstant.isSet()).isTrue();
        assertThat(dynamicConstant.get()).isEqualTo(SafeHolder.VAL);
        assertThat(dynamicConstant.get()).isEqualTo(SafeHolder.VAL);
    }

    @Test
    public void defaultShouldBeUsed_multipleTimes_ifSetIsNotInvoked() {
        String myDefault = "my default";
        DynamicConstant<String> dynamicConstant = DynamicConstant.withDefault(myDefault);
        assertThat(dynamicConstant.isSet()).isFalse();
        assertThat(dynamicConstant.get()).isEqualTo(myDefault);
        assertThat(dynamicConstant.isSet()).isTrue();
        assertThat(dynamicConstant.get()).isEqualTo(myDefault);
        assertThat(dynamicConstant.get()).isEqualTo(myDefault);
    }

    @Test
    public void getDefault_thenSet_sameValue_shouldSucceed() {
        DynamicConstant<String> dynamicConstant = DynamicConstant.lazyDefault(() -> SafeHolder.VAL);
        dynamicConstant.get();
        dynamicConstant.set(SafeHolder.VAL);
        assertThat(dynamicConstant.get()).isEqualTo(SafeHolder.VAL);
    }

    @Test
    public void getDefault_thenSet_differentValues_shouldFail() {
        DynamicConstant<String> dynamicConstant = DynamicConstant.lazyDefault(() -> SafeHolder.VAL);
        dynamicConstant.get();
        try {
            dynamicConstant.set("new_value");
            Assert.fail();
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageThat().matches("New value: .* does not match existing value: .*");
        }
    }

    @Test
    public void set_thenSet_sameValue_shouldSucceed() {
        DynamicConstant<String> dynamicConstant = DynamicConstant.lazyDefault(() -> SafeHolder.VAL);
        dynamicConstant.set("new value");
        dynamicConstant.set("new value");
        assertThat(dynamicConstant.get()).isEqualTo("new value");
    }

    @Test
    public void set_thenSet_differentValues_shouldFail() {
        DynamicConstant<String> dynamicConstant = DynamicConstant.lazyDefault(() -> SafeHolder.VAL);
        dynamicConstant.set("new value 1");
        try {
            dynamicConstant.set("new value 2");
            Assert.fail();
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageThat().matches("New value: .* does not match existing value: .*");
        }
    }

    @Test
    public void setToNull_shouldThrowException() {
        DynamicConstant<String> dynamicConstant = DynamicConstant.lazyDefault(() -> SafeHolder.VAL);
        try {
            dynamicConstant.set(null);
            Assert.fail();
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageThat().isEqualTo("Cannot set value to null");
        }
    }

    @Test
    public void noDefault_followedBySet_shouldSucceed() {
        DynamicConstant<Integer> dynamicConstant = DynamicConstant.noDefault();
        assertThat(dynamicConstant.isSet()).isFalse();
        dynamicConstant.set(29);
        assertThat(dynamicConstant.isSet()).isTrue();
        assertThat(dynamicConstant.get()).isEqualTo(29);
    }

    @Test
    public void noDefault_shouldThrowExceptionWhenCallingGet() {
        DynamicConstant<Integer> dynamicConstant = DynamicConstant.noDefault();
        try {
            dynamicConstant.get();
            Assert.fail();
        } catch (NoSuchElementException e) {
            assertThat(e).hasMessageThat().isEqualTo("No default provided, nor was any value set");
        }
    }

    @Test
    public void supplierReturnsNull_shouldThrowExceptionDuringGet() {
        DynamicConstant<Integer> dynamicConstant = DynamicConstant.lazyDefault(() -> null);
        try {
            dynamicConstant.get();
            Assert.fail();
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageThat().isEqualTo("Default value should not be null");
        }
    }

    @Test
    public void nullSupplier_shouldThrowExceptionImmediately() {
        try {
            DynamicConstant.lazyDefault(null);
            Assert.fail();
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageThat().isEqualTo("Cannot have a null supplier");
        }
    }

    @Test
    public void supplierReturnsDifferentObjectEachTime_multipleParallelGets_shouldOnlyUseSupplierOnce() {
        RandomInt.numCalls.set(0);
        DynamicConstant<Integer> constant = DynamicConstant.lazyDefault(RandomInt::get);
        int numThreads = 100;
        CountDownLatch latch = new CountDownLatch(numThreads);
        Set<Integer> values = IntStream.range(0, numThreads)
                                       .mapToObj(i -> ThreadUtilc.fork(() -> getAfterLatchCountdown(constant, latch)))
                                       .collect(ImmutableList.toImmutableList()).stream()
                                       .map(ThreadUtilc::get)
                                       .collect(Collectors.toSet());
        assertThat(values).hasSize(1);
        assertThat(RandomInt.numCalls.get()).isEqualTo(1);
    }

    private static <T> T getAfterLatchCountdown(DynamicConstant<T> constant, CountDownLatch latch) {
        try {
            latch.countDown();
            latch.await();
            return constant.get();
        } catch (InterruptedException e) {
            throw new InterruptedExceptionc(e);
        }
    }

    private static class LandmineHolder {
        static final String VAL = getVal();

        private static String getVal() {
            throw new IllegalStateException();
        }
    }

    private static class SafeHolder {
        static final String VAL = getVal();

        private static String getVal() {
            return "Holder Value";
        }
    }

    private static class RandomInt {
        private static final Random RNG = new Random();

        private static final AtomicInteger numCalls = new AtomicInteger(0);

        public static int get() {
            ThreadUtilc.fork(numCalls::incrementAndGet);
            return RNG.nextInt();
        }
    }
}
