package org.rajivprab.cava;

import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class Streamc {
    // Use Streams.stream(iterable/iterator) instead
    @Deprecated
    public static <T> Stream<T> stream(Iterable<T> iterable) {
        https://stackoverflow.com/questions/23932061/convert-iterable-to-stream-using-java-8-jdk
        return StreamSupport.stream(iterable.spliterator(), false);
    }
}
