package org.rajivprab.cava.delegator;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.*;

/**
 * Enables defining a new ExecutorService which is a thin wrapper around another ExecutorService,
 * except for a few overridden methods.
 *
 * Ie, composition over inheritance.
 *
 * Example usage: see Sava's CapacityLoggingThreadPoolExecutor
 * TODO Insert Sava web-url above
 */
public interface ExecutorServiceDelegator extends ExecutorService {
    ExecutorService getDelegate();

    @Override
    default void shutdown() {
        getDelegate().shutdown();
    }

    @Override
    default List<Runnable> shutdownNow() {
        return getDelegate().shutdownNow();
    }

    @Override
    default boolean isShutdown() {
        return getDelegate().isShutdown();
    }

    @Override
    default boolean isTerminated() {
        return getDelegate().isTerminated();
    }

    @Override
    default boolean awaitTermination(long timeout, TimeUnit unit) throws InterruptedException {
        return getDelegate().awaitTermination(timeout, unit);
    }

    @Override
    default <T> Future<T> submit(Callable<T> task) {
        return getDelegate().submit(task);
    }

    @Override
    default <T> Future<T> submit(Runnable task, T result) {
        return getDelegate().submit(task, result);
    }

    @Override
    default Future<?> submit(Runnable task) {
        return getDelegate().submit(task);
    }

    @Override
    default <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> tasks) throws InterruptedException {
        return getDelegate().invokeAll(tasks);
    }

    @Override
    default <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> tasks, long timeout, TimeUnit unit)
            throws InterruptedException {
        return getDelegate().invokeAll(tasks, timeout, unit);
    }

    @Override
    default <T> T invokeAny(Collection<? extends Callable<T>> tasks) throws InterruptedException, ExecutionException {
        return getDelegate().invokeAny(tasks);
    }

    @Override
    default <T> T invokeAny(Collection<? extends Callable<T>> tasks, long timeout, TimeUnit unit)
            throws InterruptedException, ExecutionException, TimeoutException {
        return getDelegate().invokeAny(tasks, timeout, unit);
    }

    @Override
    default void execute(Runnable command) {
        getDelegate().execute(command);
    }
}