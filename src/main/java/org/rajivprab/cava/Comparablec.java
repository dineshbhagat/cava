package org.rajivprab.cava;

/**
 * Yes, this is an extremely thin wrapper, and looks idiotic as hell.
 * But I can never fluently read a.compareTo(b), without looking at the documentation to see which numbers mean what.
 * Hence why I'm providing a readable wrapper around this integer-compare.
 * Go ahead and laugh at me. I deserve it.
 */
public class Comparablec {
    // Returns true if a > b
    public static <T> boolean isGreater(Comparable<T> a, T b) {
        return a.compareTo(b) > 0;
    }

    // Returns true if a >= b
    public static <T> boolean isGreaterOrEqual(Comparable<T> a, T b) {
        return a.compareTo(b) >= 0;
    }
}
