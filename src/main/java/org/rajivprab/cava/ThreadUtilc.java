package org.rajivprab.cava;

import org.rajivprab.cava.exception.CheckedExceptionWrapper;
import org.rajivprab.cava.exception.ExecutionExceptionc;
import org.rajivprab.cava.exception.InterruptedExceptionc;
import org.rajivprab.cava.exception.TimeoutExceptionc;

import java.time.Duration;
import java.util.concurrent.*;

/**
 * Library that provides user-friendly Thread class access
 * <p>
 * Created by rprabhakar on 12/15/15.
 */
public class ThreadUtilc {
    // Use a cached thread-pool to minimize overhead from creating a new thread every time
    // Executor will auto-spawn new threads if a cached thread is not available
    private static final ExecutorService CACHED_THREAD_POOL = Executors.newCachedThreadPool();

    // ---------- Sleep ------------

    public static void sleep(Duration duration) {
        sleep(duration.toMillis());
    }

    public static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            throw new InterruptedExceptionc(e);
        }
    }

    // ---------- Fork ------------

    public static Future<?> fork(Runnable runnable) {
        return CACHED_THREAD_POOL.submit(runnable);
    }

    public static <T> Future<T> fork(Callable<T> callable) {
        return CACHED_THREAD_POOL.submit(callable);
    }

    // ---------- Get ------------

    public static <T> T call(Callable<T> callable) {
        try {
            return callable.call();
        } catch (Exception e) {
            throw CheckedExceptionWrapper.wrapIfNeeded(e);
        }
    }

    public static <T> T get(Future<T> future) {
        return get(future, Duration.ofDays(100000));
    }

    public static <T> T get(Future<T> future, long timeoutMs) {
        return get(future, Duration.ofMillis(timeoutMs));
    }

    // InterruptedException and TimeoutException are wrapped into their respective RuntimeException wrappers
    // When encountering ExecutionException, the cause itself is thrown, with a CheckedExceptionWrapper only if needed
    // The ExecutionException is logged in the log-file, if details from the parent-stack-trace are needed
    public static <T> T get(Future<T> future, Duration duration) {
        try {
            return future.get(duration.toNanos(), TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {
            throw new InterruptedExceptionc(e);
        } catch (TimeoutException e) {
            throw new TimeoutExceptionc(e);
        } catch (ExecutionException e) {
            // Do not simply throw the cause, because that lacks the stack-trace for this get-method
            // Do not wrap e.cause inside CheckedExceptionWrapper, because we would still have above problem
            throw new ExecutionExceptionc(e);
        }
    }
}
