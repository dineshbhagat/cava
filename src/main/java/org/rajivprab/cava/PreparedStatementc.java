package org.rajivprab.cava;

import com.google.common.collect.Lists;
import org.json.JSONObject;
import org.rajivprab.cava.exception.IOExceptionc;
import org.rajivprab.cava.exception.SQLExceptionc;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Utility class relating to SQL prepared-statements
 * TODO: Figure out a good way to unit test this
 *
 * Created by rprabhakar on 10/5/15.
 */
public class PreparedStatementc {
    // Returns a list of JSONObjects, each entry corresponding to one row in the DB query results
    // Each JSONObject is a series of "column-name": "value" pairs
    // All values are Strings, even if they are ints in database
    public static List<JSONObject> executeQuery(Connection connection, String query, Object... args) {
        try {
            return parseResults(getPreparedStatement(connection, query, args).executeQuery());
        } catch (SQLException e) {
            throw new SQLExceptionc(e);
        }
    }

    // Only use this for large queries, where you do not want results buffered entirely in memory
    public static void executeQuery(Connection connection, OutputStream outputStream, String query, Object... args) {
        try {
            parseResults(getPreparedStatement(connection, query, args).executeQuery(), outputStream);
        } catch (SQLException e) {
            throw new SQLExceptionc(e);
        }
    }

    // Returns either
    // (1) the row count for SQL Data Manipulation Language (DML) statements
    // (2) 0 for SQL statements that return nothing
    public static int executeUpdate(Connection connection, String query, Object... args) {
        try {
            return getPreparedStatement(connection, query, args).executeUpdate();
        } catch (SQLException e) {
            throw new SQLExceptionc(e);
        }
    }

    public static PreparedStatement getPreparedStatement(Connection connection, String query, Object... args) {
        try {
            PreparedStatement statement = connection.prepareStatement(query);
            for (int i = 0; i < args.length; i++) {
                statement.setObject(i + 1, args[i]);
            }
            return statement;
        } catch (SQLException e) {
            throw new SQLExceptionc(e);
        }
    }

    public static List<JSONObject> parseResults(ResultSet resultSet) {
        try {
            String[] columnNames = getColumnNames(resultSet);
            List<JSONObject> results = Lists.newArrayList();
            while (resultSet.next()) {
                results.add(getEntryJson(resultSet, columnNames));
            }
            return results;
        } catch (SQLException e) {
            throw new SQLExceptionc(e);
        }
    }

    public static void parseResults(ResultSet resultSet, OutputStream outputStream) {
        try {
            String[] columnNames = getColumnNames(resultSet);
            while (resultSet.next()) {
                outputStream.write(getEntryJson(resultSet, columnNames).toString().getBytes());
                outputStream.write("\n".getBytes());
            }
            outputStream.flush();
        } catch (IOException e) {
            throw new IOExceptionc(e);
        } catch (SQLException e) {
            throw new SQLExceptionc(e);
        }
    }

    public static String[] getColumnNames(ResultSet resultSet) {
        try {
            int numColumns = resultSet.getMetaData().getColumnCount();
            String[] columnNames = new String[numColumns];
            for (int i = 0; i < numColumns; i++) {
                columnNames[i] = resultSet.getMetaData().getColumnName(i + 1);
            }
            return columnNames;
        } catch (SQLException e) {
            throw new SQLExceptionc(e);
        }
    }

    public static JSONObject getEntryJson(ResultSet resultSet, String[] columnNames) {
        try {
            JSONObject entry = new JSONObject();
            for (int i = 0; i < columnNames.length; i++) {
                String value = resultSet.getString(i + 1);
                entry.put(columnNames[i], value);
            }
            return entry;
        } catch (SQLException e) {
            throw new SQLExceptionc(e);
        }
    }
}
