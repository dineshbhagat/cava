package org.rajivprab.cava;

import com.google.common.collect.Iterators;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONPointer;

import java.io.Writer;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Should probably be using a different JSON library with built-in support for immutability.
 * But if you already have existing code built around org.json, then this provides an easy way to get immutability.
 *
 * Immutability guarantee has not been thoroughly investigated or tested. Use with caution.
 *
 * TODO Enhancement: toMap() and toList() are safe, and do not allow for backdoor modifications?
 * Triple check to make sure
 *
 * TODO Enhancement: Triple check that all methods in JSONObject are either overridden here,
 * or do not access super.map, neither directly nor indirectly. super.map is never initialized by this class.
 *
 * TODO Enhancement: Most of the put/get/opt methods are not overridden,
 * because they call the base put/opt methods, which are overridden.
 * Consider overriding all of them anyway, in case the base class implementation changes.
 *
 * TODO Unit tests
 */
public final class JSONObjectImmutable extends JSONObject {
    private static final String DEFAULT_ERROR_MESSAGE = "This JSONObject cannot be modified";

    public static JSONObjectImmutable build(JSONObject json) {
        return json instanceof JSONObjectImmutable ? (JSONObjectImmutable) json : build(json.toString());
    }

    public static JSONObjectImmutable build(String json) {
        return unmodifiable(new JSONObject(json));
    }

    /**
     * External callers can only get an instance of JSONObjectImmutable via the above build methods, which makes a deep
     * copy. This ensures that the JSONObject reference held by the caller, is isolated from the wrapper's reference.
     *
     * Internally, whenever a child JSONObject or JSONArray is retrieved, it is wrapped using
     * {@link JSONObjectImmutable#unmodifiable(JSONObject)}.
     *
     * The wrapper does not make a copy - it merely adds a wrapper preventing modification operations.
     * If an external caller was able to call the wrapper directly, then the object would no longer be immutable.
     * But because the wrap method is package private, and is only ever invoked from within JSONObjectImmutable and
     * JSONArrayImmutable, we avoid any possibility of modifications happening.
     *
     * We avoid making a copy in the wrapper, because then, every single json.getJSONObject or json.getJSONArray
     * operation would become expensive.
     */
    static JSONObjectImmutable unmodifiable(JSONObject json) {
        // Cannot simply overload this method:
        // https://stackoverflow.com/questions/1572322/overloaded-method-selection-based-on-the-parameters-real-type
        return json instanceof JSONObjectImmutable ? (JSONObjectImmutable) json : new JSONObjectImmutable(json);
    }

    static Object optImmutable(Object object) {
        if (object instanceof JSONArray) {
            return JSONArrayImmutable.unmodifiable((JSONArray) object);
        } else if (object instanceof JSONObject) {
            return JSONObjectImmutable.unmodifiable((JSONObject) object);
        } else {
            return object;
        }
    }

    private final JSONObject underlying;

    private JSONObjectImmutable(JSONObject json) {
        // Unfortunately, there's no way to simply copy an entire JSONObject, without listing out all keys.
        // Unfortunately, there's also no way to simply pass in the json or json.map directly to the super's constructor.

        // To prevent the performance loss of reconstructing the map, we avoid initializing the super constructor
        // with the json data. Instead, we store it here, override all methods that reference the instance variable
        // in the super, and call it on the wrapped json instead. Ie, composition instead of inheritance.

        // Downside: We have to implement most methods in JSONObject.
        // If any method is not implemented, either by oversight or because a new method is added later,
        // the call will pass-through to the parent class, which would have been left uninitialized.
        // This is fine if the parent class then invokes another method that is overridden here.
        // But not fine if the parent class invokes its uninitialized instance variable directly.
        this.underlying = json;
    }

    // --------- Wrap all allowed base class methods. Anything not wrapped, should call one of these methods -------

    @Override
    public Set<String> keySet() {
        return Collections.unmodifiableSet(underlying.keySet());
    }

    @Override
    public Iterator<String> keys() {
        return Iterators.unmodifiableIterator(underlying.keys());
    }

    // All other get/opt methods reference this one
    // Though if the base class implementation were to change, that would break immutability!
    @Override
    public Object opt(String key) {
        return optImmutable(underlying.opt(key));
    }

    @Override
    public boolean has(String key) {
        return underlying.has(key);
    }

    @Override
    public boolean isNull(String key) {
        return underlying.isNull(key);
    }

    @Override
    public int length() {
        return underlying.length();
    }

    @Override
    public JSONArray names() {
        return underlying.names();
    }

    @Override
    public boolean similar(Object other) {
        return underlying.similar(other);
    }

    @Override
    public Writer write(Writer writer, int indentFactor, int indent) {
        return underlying.write(writer, indentFactor, indent);
    }

    @Override
    public Map<String, Object> toMap() {
        return underlying.toMap();
    }

    // ------------- Not supported --------------

    // Queries could result in JSONObject or JSONArray leaking out
    // TODO Enhancement: Handle the above case by wrapping it, instead of not supporting this operation
    @Override
    public Object query(JSONPointer jsonPointer) {
        throw new UnsupportedOperationException(DEFAULT_ERROR_MESSAGE);
    }

    @Override
    public Object optQuery(String jsonPointer) {
        throw new UnsupportedOperationException(DEFAULT_ERROR_MESSAGE);
    }

    @Override
    public JSONObject put(String key, Object value) {
        throw new UnsupportedOperationException(DEFAULT_ERROR_MESSAGE);
    }

    @Override
    public JSONObject accumulate(String key, Object value) {
        throw new UnsupportedOperationException(DEFAULT_ERROR_MESSAGE);
    }

    @Override
    public JSONObject append(String key, Object value) {
        throw new UnsupportedOperationException(DEFAULT_ERROR_MESSAGE);
    }

    @Override
    public JSONObject increment(String key) {
        throw new UnsupportedOperationException(DEFAULT_ERROR_MESSAGE);
    }

    @Override
    public Object remove(String key) {
        throw new UnsupportedOperationException(DEFAULT_ERROR_MESSAGE);
    }
}
