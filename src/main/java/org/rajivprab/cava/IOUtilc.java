package org.rajivprab.cava;

import com.google.common.base.Charsets;
import org.apache.commons.io.IOUtils;
import org.rajivprab.cava.exception.IOExceptionc;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.List;

/**
 * Generic utilities related to IO operations
 * <p>
 * Created by rprabhakar on 12/15/15.
 */
public class IOUtilc {
    public static final Charset CHARSET_USED = Charsets.UTF_8;

    public static String toString(InputStream inputStream) {
        try {
            return IOUtils.toString(inputStream, CHARSET_USED);
        } catch (IOException e) {
            throw new IOExceptionc(e);
        }
    }

    public static List<String> readLines(InputStream inputStream) {
        try {
            return IOUtils.readLines(inputStream, CHARSET_USED);
        } catch (IOException e) {
            throw new IOExceptionc(e);
        }
    }
}
