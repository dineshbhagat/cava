package org.rajivprab.cava;

import com.google.common.collect.Streams;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.*;
import java.util.stream.Stream;

/**
 * TODO Enhancement: Database optimizations:
 * No select *
 * No triggers
 * Check for missing indexes
 * <p>
 * Utilities for common state-tasks.
 * All methods are guaranteed not to alter the input objects.
 * <p>
 * TODO Enhancement: Contribute to org.json
 * <p>
 * Created by rprabhakar on 7/9/15.
 */
public class JsonUtilc {
    public static boolean equals(JSONArray array1, JSONArray array2) {
        if (array1.length() != array2.length()) { return false; }
        for (int i = 0; i < array1.length(); i++) {
            Object value1 = array1.get(i);
            Object value2 = array2.get(i);
            if (!equals(value1, value2)) { return false; }
        }
        return true;
    }

    public static boolean equals(JSONObject object1, JSONObject object2) {
        Set<String> keys1 = object1.keySet();
        Set<String> keys2 = object2.keySet();
        if (!keys1.equals(keys2)) { return false; }
        for (String key : keys1) {
            Object value1 = object1.get(key);
            Object value2 = object2.get(key);
            if (!equals(value1, value2)) { return false; }
        }
        return true;
    }

    public static boolean equals(Object value1, Object value2) {
        if (JSONObject.class.isInstance(value1) && JSONObject.class.isInstance(value2)) {
            return equals((JSONObject) value1, (JSONObject) value2);
        } else if (JSONArray.class.isInstance(value1) && JSONArray.class.isInstance(value2)) {
            return equals((JSONArray) value1, (JSONArray) value2);
        } else {
            return value1.equals(value2);
        }
    }

    public static <T> JSONArray mergeAllObjectsIntoArray(Collection<T> objects) {
        JSONArray output = new JSONArray();
        for (T object : objects) {
            output.put(object);
        }
        return output;
    }

    // Returns a non-thread-safe lazy-iterable with fixed memory footprint
    // Any type errors will produce run-time-exceptions, just like the jsonArray.get* method calls
    // Any changes to the underlying jsonArray, could be reflected in the iterator's behavior, or cause it to fail
    public static <T> Iterable<T> getIterable(JSONArray jsonArray) {
        // Cannot use array.toList, because that converts all JSONObjects into Map<String, Object>
        return () -> new JsonArrayIterator<>(jsonArray);
    }

    // Same as getIterable, except that it returns a stream of the iterator
    public static <T> Stream<T> getStream(JSONArray jsonArray) {
        return Streams.stream(getIterable(jsonArray));
    }

    private static class JsonArrayIterator<T> implements Iterator<T> {
        private final JSONArray array;
        private final int length;

        private int nextIndex = 0;

        public JsonArrayIterator(JSONArray array) {
            this.array = array;
            this.length = array.length();
        }

        @Override
        public boolean hasNext() {
            Validatec.equals(length, array.length(), ConcurrentModificationException.class, "JSONArray length has changed");
            return length > nextIndex;
        }

        @Override
        @SuppressWarnings("unchecked")
        public T next() {
            Validatec.isTrue(hasNext(), NoSuchElementException.class, "Next element does not exist");
            nextIndex++;
            return (T) array.get(nextIndex - 1);
        }
    }
}
