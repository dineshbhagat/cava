package org.rajivprab.cava;

import org.apache.commons.lang3.Validate;

import java.util.NoSuchElementException;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Supplier;

/**
 * Constant: all calls to get() are guaranteed to return the same value.
 *
 * Dynamic: The default value is lazily constructed/retrieved during the first get() call,
 * and only if no other value was previously set.
 *
 * Before the first get() call, the data can be set() to any arbitrary value.
 * It is the caller's responsibility to ensure that any calls to set() match any previous set() or get() calls made.
 * Else, an exception will be thrown, and the newly set value will be ignored.
 *
 * This class does not support setting/getting null values.
 * It's ok for no default to be given, or for the default-supplier to return a null,
 * iff a non-null value is set(), prior to any get() calls.
 *
 * This class is intended to be used in cases where the set() call might be needed.
 * Otherwise, just use the lazy-singleton pattern: https://en.wikipedia.org/wiki/Initialization-on-demand_holder_idiom
 *
 * This class is thread-safe.
 */
public class DynamicConstant<T> {
    // Using AtomicReference to ensure thread-safety and visibility across parallel gets/sets
    // https://stackoverflow.com/questions/14676997/java-memory-visibility-and-atomicreferences
    // https://stackoverflow.com/questions/3964211/when-to-use-atomicreference-in-java
    private final AtomicReference<T> value = new AtomicReference<>();
    private final Supplier<T> defaultSupplier;

    private T defaultSupplied;

    // ------------- Constructors -----------

    public static <T> DynamicConstant<T> noDefault() {
        return lazyDefault(() -> {throw new NoSuchElementException("No default provided, nor was any value set");});
    }

    public static <T> DynamicConstant<T> withDefault(T def) {
        return new DynamicConstant<>(() -> def);
    }

    public static <T> DynamicConstant<T> lazyDefault(Supplier<T> defaultSupplier) {
        return new DynamicConstant<>(defaultSupplier);
    }

    private DynamicConstant(Supplier<T> defaultSupplier) {
        this.defaultSupplier = Validatec.notNull(defaultSupplier, "Cannot have a null supplier");
    }

    // --------------------------------------

    // Returns the most recently set value, or if never set, the default.
    // Throws NoSuchElementException if default was not provided, and set was never called.
    public T get() {
        T value = this.value.get();
        return value == null ? setDefault() : value;
    }

    // Returns true if the value has been permanently set to any user-supplied value, or the default
    // If value has never been set, the default is not invoked
    public boolean isSet() {
        return value.get() != null;
    }

    // If set is called with data that is incompatible with the previous get/set calls,
    // An exception will be thrown, and no data will be changed.
    public T set(T newVal) {
        Validatec.notNull(newVal, "Cannot set value to null");
        boolean updated = value.compareAndSet(null, newVal);
        Validate.isTrue(updated || value.get().equals(newVal),
                        "New value: %s, does not match existing value: %s", newVal, value.get());
        return newVal;
    }

    private synchronized T setDefault() {
        if (defaultSupplied == null) {
            defaultSupplied = Validatec.notNull(defaultSupplier.get(), "Default value should not be null");
            return set(defaultSupplied);
        } else {
            return defaultSupplied;
        }
    }
}