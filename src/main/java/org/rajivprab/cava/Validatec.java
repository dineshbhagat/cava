package org.rajivprab.cava;

import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.javatuples.Triplet;
import org.rajivprab.cava.exception.CheckedExceptionWrapper;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.function.Supplier;

/**
 * Validate library supplement.
 * Allows throwing user-specified exceptions.
 * Additional checking methods added, for equals, collection utilities, and other such cases.
 * <p>
 * Created by rprabhakar on 12/15/15.
 */
public class Validatec {
    private static final Logger log = LogManager.getLogger(Validatec.class);
    private static final Class<? extends RuntimeException> DEFAULT_EXCEPTION = IllegalArgumentException.class;

    // ---------------------------------------- Is true -------------------------------------------

    public static void isTrue(boolean bool) {
        isTrue(bool, DEFAULT_EXCEPTION);
    }

    public static void isTrue(boolean bool, String message) {
        isTrue(bool, DEFAULT_EXCEPTION, message);
    }

    public static void isTrue(boolean bool, Class<? extends RuntimeException> exceptionType) {
        isTrue(bool, exceptionType, "Should be true");
    }

    public static void isTrue(boolean bool, Class<? extends RuntimeException> exceptionType, String message) {
        isTrue(bool, () -> buildException(message, exceptionType));
    }

    public static void isTrue(boolean bool, Supplier<RuntimeException> exception) {
        if (!bool) {
            throw exception.get();
        }
    }

    // ---------------------------------------- Is false -------------------------------------------

    public static void isFalse(boolean bool) {
        isFalse(bool, DEFAULT_EXCEPTION);
    }

    public static void isFalse(boolean bool, String message) {
        isFalse(bool, DEFAULT_EXCEPTION, message);
    }

    public static void isFalse(boolean bool, Class<? extends RuntimeException> exceptionType) {
        isFalse(bool, exceptionType, "Should be false");
    }

    public static void isFalse(boolean bool, Class<? extends RuntimeException> exceptionType, String message) {
        isFalse(bool, () -> buildException(message, exceptionType));
    }

    public static void isFalse(boolean bool, Supplier<RuntimeException> exception) {
        if (bool) {
            throw exception.get();
        }
    }

    // ---------------------------------------- Size -------------------------

    public static void size(Collection collection, int size) {
        size(collection, size, DEFAULT_EXCEPTION);
    }

    public static void size(Collection collection, int size, String message) {
        size(collection, size, DEFAULT_EXCEPTION, message);
    }

    public static void size(Collection collection, int size, Class<? extends RuntimeException> exceptionType) {
        size(collection, size, () -> buildException(getSizeMessage(collection, size), exceptionType));
    }

    public static void size(Collection collection, int size, Class<? extends RuntimeException> exceptionType, String msg) {
        size(collection, size, () -> buildException(msg, exceptionType));
    }

    public static void size(Collection collection, int size, Supplier<RuntimeException> exception) {
        if (collection == null || collection.size() != size) {
            log.error(getSizeMessage(collection, size));
            throw exception.get();
        }
    }

    // GetMessage methods like this one, can require O(N) time to generate the message.
    // Make sure this only gets invoked if the check fails, and not if the check succeeds.
    private static String getSizeMessage(Collection collection, int size) {
        return collection + " should have " + size + " entries";
    }

    // ---------------------------------------- Array.length -------------------------

    public static void length(Object[] array, int length) {
        length(array, length, DEFAULT_EXCEPTION);
    }

    public static void length(Object[] array, int length, String message) {
        length(array, length, DEFAULT_EXCEPTION, message);
    }

    public static void length(Object[] array, int length, Class<? extends RuntimeException> exceptionType) {
        length(array, length, () -> buildException(getArrayLengthMessage(array, length), exceptionType));
    }

    public static void length(Object[] array, int length, Class<? extends RuntimeException> type, String message) {
        length(array, length, () -> buildException(message, type));
    }

    public static void length(Object[] array, int length, Supplier<RuntimeException> exception) {
        if (array == null || array.length != length) {
            log.error(getArrayLengthMessage(array, length));
            throw exception.get();
        }
    }

    private static String getArrayLengthMessage(Object[] array, int length) {
        return Arrays.toString(array) + " should have " + length + " entries";
    }

    // ---------------------------------------- String.length -------------------------

    public static void length(String string, int length) {
        length(string, length, DEFAULT_EXCEPTION);
    }

    public static void length(String string, int length, String message) {
        length(string, length, DEFAULT_EXCEPTION, message);
    }

    public static void length(String string, int length, Class<? extends RuntimeException> exceptionType) {
        length(string, length, () -> buildException(getStringLengthMessage(string, length), exceptionType));
    }

    public static void length(String string, int length, Class<? extends RuntimeException> exceptionType, String message) {
        length(string, length, () -> buildException(message, exceptionType));
    }

    public static void length(String string, int length, Supplier<RuntimeException> exception) {
        if (string == null || string.length() != length) {
            log.error(getStringLengthMessage(string, length));
            throw exception.get();
        }
    }

    private static String getStringLengthMessage(String string, int length) {
        return wrap(string) + " should be " + length + " characters long";
    }

    // ---------------------------------------- String.matches --------------------------

    public static void matches(String string, String regex) {
        matches(string, regex, DEFAULT_EXCEPTION);
    }

    public static void matches(String string, String regex, String message) {
        matches(string, regex, DEFAULT_EXCEPTION, message);
    }

    public static void matches(String string, String regex, Class<? extends RuntimeException> exceptionType) {
        matches(string, regex, () -> buildException(getStringMatchesMessage(string, regex), exceptionType));
    }

    public static void matches(String string, String regex, Class<? extends RuntimeException> exceptionType, String message) {
        matches(string, regex, () -> buildException(message, exceptionType));
    }

    public static void matches(String string, String regex, Supplier<RuntimeException> exception) {
        if (regex == null || string == null || !string.matches(regex)) {
            log.error(getStringMatchesMessage(string, regex));
            throw exception.get();
        }
    }

    private static String getStringMatchesMessage(String string, String regex) {
        return wrap(string) + " should match " + wrap(regex);
    }

    // ---------------------------------------- Greater than ----------------

    public static <T> void greaterThan(Comparable<T> a, T b) {
        greaterThan(a, b, DEFAULT_EXCEPTION);
    }

    public static <T> void greaterThan(Comparable<T> a, T b, String message) {
        greaterThan(a, b, DEFAULT_EXCEPTION, message);
    }

    public static <T> void greaterThan(Comparable<T> a, T b, Class<? extends RuntimeException> exceptionType) {
        greaterThan(a, b, () -> buildException(getGreaterThanMessage(a, b), exceptionType));
    }

    public static <T> void greaterThan(Comparable<T> a, T b, Class<? extends RuntimeException> exceptionType, String message) {
        greaterThan(a, b, () -> buildException(message, exceptionType));
    }

    public static <T> void greaterThan(Comparable<T> a, T b, Supplier<RuntimeException> exception) {
        if (a == null || b == null || !Comparablec.isGreater(a, b)) {
            log.error(getGreaterThanMessage(a, b));
            throw exception.get();
        }
    }

    private static <T> String getGreaterThanMessage(Comparable<T> a, T b) {
        return a + " should be greater than " + b;
    }

    // ---------------------------------------- Greater than or equal ----------------

    public static <T> void greaterOrEqual(Comparable<T> a, T b) {
        greaterOrEqual(a, b, DEFAULT_EXCEPTION);
    }

    public static <T> void greaterOrEqual(Comparable<T> a, T b, String message) {
        greaterOrEqual(a, b, DEFAULT_EXCEPTION, message);
    }

    public static <T> void greaterOrEqual(Comparable<T> a, T b, Class<? extends RuntimeException> exceptionType) {
        greaterOrEqual(a, b, () -> buildException(getGreaterOrEqualMessage(a, b), exceptionType));
    }

    public static <T> void greaterOrEqual(Comparable<T> a, T b,
                                          Class<? extends RuntimeException> exceptionType, String message) {
        greaterOrEqual(a, b, () -> buildException(message, exceptionType));
    }

    public static <T> void greaterOrEqual(Comparable<T> a, T b, Supplier<RuntimeException> exception) {
        if (a == null || b == null || !Comparablec.isGreaterOrEqual(a, b)) {
            log.error(getGreaterOrEqualMessage(a, b));
            throw exception.get();
        }
    }

    private static <T> String getGreaterOrEqualMessage(Comparable<T> a, T b) {
        return a + " should be greater than or equal to " + b;
    }

    // ---------------------------------------- Equals -------------------------

    public static void equals(Object a, Object b) {
        equals(a, b, DEFAULT_EXCEPTION);
    }

    public static void equals(Object a, Object b, String message) {
        equals(a, b, DEFAULT_EXCEPTION, message);
    }

    public static void equals(Object a, Object b, Class<? extends RuntimeException> exceptionType) {
        equals(a, b, () -> buildException(equalsErrorMessage(a, b), exceptionType));
    }

    public static void equals(Object a, Object b, Class<? extends RuntimeException> exceptionType, String message) {
        equals(a, b, () -> buildException(message, exceptionType));
    }

    public static void equals(Object a, Object b, Supplier<RuntimeException> exception) {
        if (a == null || !a.equals(b)) {
            log.error(equalsErrorMessage(a, b));
            throw exception.get();
        }
    }

    private static String equalsErrorMessage(Object a, Object b) {
        return a + ", should equal: " + b;
    }

    // ---------------------------------------- Not Equals ----------------------------------------

    public static void notEquals(Object a, Object b) {
        notEquals(a, b, DEFAULT_EXCEPTION);
    }

    public static void notEquals(Object a, Object b, String message) {
        notEquals(a, b, DEFAULT_EXCEPTION, message);
    }

    public static void notEquals(Object a, Object b, Class<? extends RuntimeException> exceptionType) {
        notEquals(a, b, () -> buildException(notEqualsErrorMessage(a, b), exceptionType));
    }

    public static void notEquals(Object a, Object b, Class<? extends RuntimeException> exceptionType, String message) {
        notEquals(a, b, () -> buildException(message, exceptionType));
    }

    public static void notEquals(Object a, Object b, Supplier<RuntimeException> exception) {
        if (a == null || b == null || a.equals(b)) {
            log.error(notEqualsErrorMessage(a, b));
            throw exception.get();
        }
    }

    private static String notEqualsErrorMessage(Object a, Object b) {
        return a + ", should not equal: " + b;
    }

    // ---------------------------------------- Null --------------------------

    public static void isNull(Object object) {
        isNull(object, DEFAULT_EXCEPTION);
    }

    public static void isNull(Object object, String message) {
        isNull(object, DEFAULT_EXCEPTION, message);
    }

    public static void isNull(Object object, Class<? extends RuntimeException> exceptionType) {
        isNull(object, () -> buildException(getNullMessage(object), exceptionType));
    }

    public static void isNull(Object object, Class<? extends RuntimeException> exceptionType, String message) {
        isNull(object, () -> buildException(message, exceptionType));
    }

    public static void isNull(Object object, Supplier<RuntimeException> exception) {
        if (object != null) {
            log.error(getNullMessage(object));
            throw exception.get();
        }
    }

    private static String getNullMessage(Object object) {
        return object + " should be null";
    }

    // ---------------------------------------- Not Null ----------------------------------------

    private static final String NOT_NULL_MESSAGE = "Argument should not be null";

    public static <T> T notNull(T arg) {
        return notNull(arg, DEFAULT_EXCEPTION);
    }

    public static <T> T notNull(T arg, String message) {
        return notNull(arg, DEFAULT_EXCEPTION, message);
    }

    public static <T> T notNull(T arg, Class<? extends RuntimeException> exceptionType) {
        return notNull(arg, exceptionType, NOT_NULL_MESSAGE);
    }

    public static <T> T notNull(T arg, Class<? extends RuntimeException> exceptionType, String message) {
        return notNull(arg, () -> buildException(message, exceptionType));
    }

    public static <T> T notNull(T arg, Supplier<RuntimeException> exception) {
        if (arg == null) {
            log.error(NOT_NULL_MESSAGE);
            throw exception.get();
        }
        return arg;
    }

    // ----------------------------------- NoNulls - Vararg -------------------------------------

    public static void noNulls(Object... objects) {
        noNulls(() -> buildException(getNoNullsErrorMessage(objects), DEFAULT_EXCEPTION), objects);
    }

    // Do not provide any other helper methods, because the parameters can be confused with vararg parameters easily
    public static void noNulls(Supplier<RuntimeException> exception, Object... objects) {
        Validatec.notNull(exception);
        if (objects == null) {
            log.error("Null array passed in");
            throw exception.get();
        }
        for (Object element : objects) {
            if (element == null) {
                log.error(getNoNullsErrorMessage(objects));
                throw exception.get();
            }
        }
    }

    private static String getNoNullsErrorMessage(Object... objects) {
        return Arrays.toString(objects) + " should not contain any nulls";
    }

    // ----------------------------------- NoNulls - Collection -------------------------------------

    public static <T> void noNullsIn(Iterable<T> iterable) {
        noNullsIn(iterable, DEFAULT_EXCEPTION);
    }

    public static <T> void noNullsIn(Iterable<T> iterable, String message) {
        noNullsIn(iterable, () -> buildException(message, DEFAULT_EXCEPTION));
    }

    public static <T> void noNullsIn(Iterable<T> iterable, Class<? extends RuntimeException> exceptionType) {
        // Only construct the error message lazily
        noNullsIn(iterable, () -> buildException(getNoNullsInErrorMessage(iterable), exceptionType));
    }

    public static <T> void noNullsIn(Iterable<T> iterable, Class<? extends RuntimeException> exceptionType, String message) {
        noNullsIn(iterable, () -> buildException(message, exceptionType));
    }

    public static <T> void noNullsIn(Iterable<T> iterable, Supplier<RuntimeException> exception) {
        if (iterable == null) {
            log.error("Null iterable passed in");
            throw exception.get();
        }
        for (T element : iterable) {
            if (element == null) {
                log.error(getNoNullsInErrorMessage(iterable));
                throw exception.get();
            }
        }
    }

    private static <T> String getNoNullsInErrorMessage(Iterable<T> objects) {
        return objects + " should not contain any nulls";
    }

    // ----------------------------------------- String Contains -----------------------------------------------

    public static void contains(String string, String subString) {
        contains(string, subString, DEFAULT_EXCEPTION);
    }

    public static void contains(String string, String subString, String message) {
        contains(string, subString, DEFAULT_EXCEPTION, message);
    }

    public static void contains(String string, String subString, Class<? extends RuntimeException> exceptionType) {
        contains(string, subString, () -> buildException(stringContainsErrorMessage(string, subString), exceptionType));
    }

    public static void contains(String string, String subString,
                                Class<? extends RuntimeException> exceptionType, String message) {
        contains(string, subString, () -> buildException(message, exceptionType));
    }

    public static void contains(String string, String subString, Supplier<RuntimeException> exception) {
        if (string == null || subString == null || !string.contains(subString)) {
            log.error(stringContainsErrorMessage(string, subString));
            throw exception.get();
        }
    }

    private static String stringContainsErrorMessage(String string, String subString) {
        return wrap(string) + " should contain " + wrap(subString);
    }

    // --------------------------------------- Collection contains entry -------------------------------------------

    public static <T> void contains(Collection<T> collection, T value) {
        contains(collection, value, DEFAULT_EXCEPTION);
    }

    public static <T> void contains(Collection<T> collection, T value, String message) {
        contains(collection, value, DEFAULT_EXCEPTION, message);
    }

    public static <T> void contains(Collection<T> collection, T value, Class<? extends RuntimeException> exceptionType) {
        contains(collection, value, () -> buildException(collectionContainsErrorMessage(collection, value), exceptionType));
    }

    public static <T> void contains(Collection<T> collection, T value,
                                    Class<? extends RuntimeException> exceptionType, String message) {
        contains(collection, value, () -> buildException(message, exceptionType));
    }

    public static <T> void contains(Collection<T> collection, T value, Supplier<RuntimeException> exception) {
        if (collection == null || value == null || !collection.contains(value)) {
            log.error(collectionContainsErrorMessage(collection, value));
            throw exception.get();
        }
    }

    private static <T> String collectionContainsErrorMessage(Collection<T> collection, T value) {
        return collection + " should contain " + value;
    }

    // ---------------------------- Collection does not contain entry -------------------------

    public static <T> void doesNotContain(Collection<T> collection, T value) {
        doesNotContain(collection, value, DEFAULT_EXCEPTION);
    }

    public static <T> void doesNotContain(Collection<T> collection, T value, String message) {
        doesNotContain(collection, value, DEFAULT_EXCEPTION, message);
    }

    public static <T> void doesNotContain(Collection<T> collection, T value, Class<? extends RuntimeException> exception) {
        doesNotContain(collection, value, () -> buildException(collectionDoesNotContainsError(collection, value), exception));
    }

    public static <T> void doesNotContain(Collection<T> collection, T value,
                                          Class<? extends RuntimeException> exception, String message) {
        doesNotContain(collection, value, () -> buildException(message, exception));
    }

    public static <T> void doesNotContain(Collection<T> collection, T value, Supplier<RuntimeException> exception) {
        if (collection == null || value == null || collection.contains(value)) {
            log.error(collectionDoesNotContainsError(collection, value));
            throw exception.get();
        }
    }

    private static <T> String collectionDoesNotContainsError(Collection<T> collection, T value) {
        return collection + " should not contain " + value;
    }

    // ---------------------------------------- Not Empty: Collection ----------------------------------------

    public static <T> void notEmpty(Collection<T> collection) {
        notEmpty(collection, DEFAULT_EXCEPTION);
    }

    public static <T> void notEmpty(Collection<T> collection, String message) {
        notEmpty(collection, DEFAULT_EXCEPTION, message);
    }

    public static <T> void notEmpty(Collection<T> collection, Class<? extends RuntimeException> exceptionType) {
        notEmpty(collection, () -> buildException(getCollectionNotEmptyMessage(collection), exceptionType));
    }

    public static <T> void notEmpty(Collection<T> collection, Class<? extends RuntimeException> exceptionType, String msg) {
        notEmpty(collection, () -> buildException(msg, exceptionType));
    }

    public static <T> void notEmpty(Collection<T> collection, Supplier<RuntimeException> exception) {
        if (collection == null || collection.isEmpty()) {
            log.error(getCollectionNotEmptyMessage(collection));
            throw exception.get();
        }
    }

    private static String getCollectionNotEmptyMessage(Collection collection) {
        return collection + " should not be empty";
    }

    // ---------------------------------------- Not Empty: String --------------------------

    public static String notEmpty(String string) {
        return notEmpty(string, DEFAULT_EXCEPTION);
    }

    public static String notEmpty(String string, String message) {
        return notEmpty(string, DEFAULT_EXCEPTION, message);
    }

    public static String notEmpty(String string, Class<? extends RuntimeException> exceptionType) {
        return notEmpty(string, () -> buildException(getStringNotEmptyMessage(string), exceptionType));
    }

    public static String notEmpty(String string, Class<? extends RuntimeException> exceptionType, String message) {
        return notEmpty(string, () -> buildException(message, exceptionType));
    }

    public static String notEmpty(String string, Supplier<RuntimeException> exception) {
        if (Strings.isNullOrEmpty(string)) {
            log.error(getStringNotEmptyMessage(string));
            throw exception.get();
        }
        return string;
    }

    private static String getStringNotEmptyMessage(String string) {
        return wrap(string) + " should not be null or empty";
    }

    // --------------------------- NoneEmpty: Strings[] -------------------------------

    public static void noneEmpty(String... strings) {
        noneEmpty(() -> buildException(getNoneEmptyMessage(strings), DEFAULT_EXCEPTION), strings);
    }

    // No other helpers provided, to avoid conflict with varargs
    public static void noneEmpty(Supplier<RuntimeException> exception, String... strings) {
        if (strings == null || strings.length == 0) {
            log.error(getNoneEmptyMessage(strings));
            throw exception.get();
        }
        if (strings.length == 1) {
            log.error("For single-string check, use other notEmpty method. Found: " + strings[0]);
        }
        for (String string : strings) {
            if (Strings.isNullOrEmpty(string)) {
                log.error(getNoneEmptyMessage(strings));
                throw exception.get();
            }
        }
    }

    private static String getNoneEmptyMessage(String... strings) {
        return Arrays.toString(strings) + " should not be empty, and should not contain null/empty strings";
    }

    // --------------------------- NoneEmpty: Iterable<Strings> -------------------------------

    public static void noneEmpty(Iterable<String> strings) {
        noneEmpty(strings, DEFAULT_EXCEPTION);
    }

    public static void noneEmpty(Iterable<String> strings, Class<? extends RuntimeException> exceptionType) {
        noneEmpty(strings, () -> buildException(getNoneEmptyMessage(strings), exceptionType));
    }

    public static void noneEmpty(Iterable<String> strings, Class<? extends RuntimeException> exceptionType, String message) {
        noneEmpty(strings, () -> buildException(message, exceptionType));
    }

    public static void noneEmpty(Iterable<String> strings, Supplier<RuntimeException> exception) {
        if (strings == null) {
            log.error("noneEmpty should not be called with a null iterable");
            throw exception.get();
        }
        for (String string : strings) {
            if (Strings.isNullOrEmpty(string)) {
                log.error(getNoneEmptyMessage(strings));
                throw exception.get();
            }
        }
    }

    private static String getNoneEmptyMessage(Iterable<String> strings) {
        return strings + " should not contain null/empty strings";
    }

    // ---------------------------------------- Map contains key, value -------------------------

    public static <K, V> void contains(Map<K, V> map, K key, V value) {
        contains(map, key, value, DEFAULT_EXCEPTION);
    }

    public static <K, V> void contains(Map<K, V> map, K key, V value, String message) {
        contains(map, key, value, DEFAULT_EXCEPTION, message);
    }

    public static <K, V> void contains(Map<K, V> map, K key, V value, Class<? extends RuntimeException> exceptionType) {
        contains(map, key, value, () -> buildException(getMapContainsMessage(map, key, value), exceptionType));
    }

    public static <K, V> void contains(Map<K, V> map, K key, V value,
                                       Class<? extends RuntimeException> exceptionType, String message) {
        contains(map, key, value, () -> buildException(message, exceptionType));
    }

    public static <K, V> void contains(Map<K, V> map, K key, V value, Supplier<RuntimeException> exception) {
        if (map == null || key == null || value == null) {
            log.error("Found null argument for <Map, Key, Value>: " + Triplet.with(map, key, value));
            throw exception.get();
        } else if (!value.equals(map.get(key))) {
            log.error(getMapContainsMessage(map, key, value));
            throw exception.get();
        }
    }

    private static <K, V> String getMapContainsMessage(Map<K, V> map, K key, V value) {
        return map.entrySet() + " should contain: " + Maps.immutableEntry(key, value);
    }

    // ---------------------------------------- Helpers -------------------------

    private static String wrap(String arg) {
        return arg == null ? "null" : "'" + arg + "'";
    }

    // Enables lazy construction of exception, only if it will be thrown
    private static RuntimeException buildException(String message, Class<? extends RuntimeException> exceptionType) {
        try {
            return exceptionType.getConstructor(String.class).newInstance(Strings.nullToEmpty(message));
        } catch (ReflectiveOperationException e) {
            throw CheckedExceptionWrapper.wrapIfNeeded(e);
        }
    }
}
