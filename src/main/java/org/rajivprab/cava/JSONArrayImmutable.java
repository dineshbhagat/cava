package org.rajivprab.cava;

import com.google.common.collect.Iterators;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONPointer;

import java.io.Writer;
import java.util.Iterator;
import java.util.List;

/**
 * Refer to javadoc for {@link JSONObjectImmutable}
 *
 * TODO Unit tests
 */
public final class JSONArrayImmutable extends JSONArray {
    private static final String DEFAULT_ERROR_MESSAGE = "This JSONArray cannot be modified";

    public static JSONArrayImmutable build(JSONArray json) {
        return json instanceof JSONArrayImmutable ? (JSONArrayImmutable) json : build(json.toString());
    }

    public static JSONArrayImmutable build(String json) {
        return unmodifiable(new JSONArray(json));
    }

    /** @see JSONObjectImmutable#unmodifiable(JSONObject) */
    static JSONArrayImmutable unmodifiable(JSONArray json) {
        return json instanceof JSONArrayImmutable ? (JSONArrayImmutable) json : new JSONArrayImmutable(json);
    }

    private final JSONArray underlying;

    private JSONArrayImmutable(JSONArray json) {
        // Same comments apply as the JSONObjectImmutable constructor
        this.underlying = json;
    }

    // --------- Wrap all allowed base class methods. Anything not wrapped, should call one of these methods -------

    @Override
    public Iterator<Object> iterator() {
        return Iterators.unmodifiableIterator(underlying.iterator());
    }

    @Override
    public Object opt(int index) {
        return JSONObjectImmutable.optImmutable(underlying.opt(index));
    }

    @Override
    public boolean isNull(int index) {
        return underlying.isNull(index);
    }

    @Override
    public String join(String separator) throws JSONException {
        return underlying.join(separator);
    }

    @Override
    public int length() {
        return underlying.length();
    }

    @Override
    public boolean similar(Object other) {
        return underlying.similar(other);
    }

    @Override
    public JSONObject toJSONObject(JSONArray names) throws JSONException {
        return underlying.toJSONObject(names);
    }

    @Override
    public Writer write(Writer writer, int indentFactor, int indent) throws JSONException {
        return underlying.write(writer, indentFactor, indent);
    }

    @Override
    public List<Object> toList() {
        return underlying.toList();
    }


    // ------------- Not supported --------------

    // Queries could result in JSONObject or JSONArray leaking out
    // TODO Enhancement: Handle the above case by wrapping it in an immutable, instead of not supporting this operation
    @Override
    public Object query(JSONPointer jsonPointer) {
        throw new UnsupportedOperationException(DEFAULT_ERROR_MESSAGE);
    }

    @Override
    public Object optQuery(JSONPointer jsonPointer) {
        throw new UnsupportedOperationException(DEFAULT_ERROR_MESSAGE);
    }

    @Override
    public Object remove(int index) {
        throw new UnsupportedOperationException(DEFAULT_ERROR_MESSAGE);
    }

    @Override
    public JSONArray put(Object value) {
        throw new UnsupportedOperationException(DEFAULT_ERROR_MESSAGE);
    }

    @Override
    public JSONArray put(int index, Object value) {
        throw new UnsupportedOperationException(DEFAULT_ERROR_MESSAGE);
    }
}
