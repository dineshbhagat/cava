Deployment instructions
=======================

Populate ~/.m2/settings.xml with:

```
<settings>
  <servers>
    <server>
      <id>ossrh</id>
      <username>{sonatype.org user-name}</username>
      <password>{sonatype.org password}</password>
    </server>
  </servers>
</settings>
```

Run: ```mvn clean deploy```


GPG Instructions
================

[Detailed guide](http://central.sonatype.org/pages/working-with-pgp-signatures.html)


If needed, ensure that GnuPG is installed.
```
Name: Rajiv Prabhakar
Email: {gitlab email}
Password: {laptop password}
```

If hitting error saying *"Signing failed: Inappropriate ioctl for device"*, run:

```export GPG_TTY=$(tty)```

Versioning
==========
Versioning generally follows [semantic versioning](https://semver.org/) rules.

One caveat: All releases with a minor version of 0, shall be considered to be in beta. As such, successive beta versions may contain breaking changes. As soon as the minor version is incremented to 1 or higher, all other semver conventions shall be followed.

Ie, version 2.0.3 may not be backward compatible with 2.0.2. But version 2.2.3 will backward compatible with both 2.2.2 and 2.1.0.

This is done to enable thorough testing of beta versions by external projects, which are unable to rely on non-semver versions.

Pending Tasks
=========
More unit tests. Most code in this library has been thoroughly tested by the [Caucus](www.thecaucus.net) project, but for more thorough testing, and to aid in future project development, it will be good to have more unit tests integrated into this repository.

Repository cleanup of various sorts.
